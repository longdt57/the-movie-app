package com.example.eastagile.themoviedatabase.home;

import com.example.eastagile.themoviedatabase.home.fragment.MovieTabContract;
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabPresenter;
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabType;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.schedulers.TestScheduler;

import static com.example.eastagile.themoviedatabase.TestUtils.createSingleMovie;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieTabPresenterLoginTest {

    private final DatabaseError databaseError = DatabaseError.fromCode(DatabaseError.NETWORK_ERROR);
    @Mock
    private MovieRepoImpl mMovieRepo;
    @Mock
    private MovieTabContract.View mView;
    @Mock
    private DatabaseReference reference;
    private MovieTabPresenter mPresenter;
    private TestScheduler mTestScheduler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mTestScheduler = new TestScheduler();
        mPresenter = new MovieTabPresenter(mMovieRepo, mTestScheduler, mTestScheduler);
        mPresenter.start(mView);
    }

    @Test
    public void getMyFavoriteMovies_Login_Test() {
        loginMock();
        mPresenter.loadMovies(MovieTabType.MY_FAVORITE, false, true, reference);
        mTestScheduler.triggerActions();

        verify(mView).showMovies(argThat(argument -> argument.size() == 0));
    }

    @Test
    public void getMyFavoriteMovies_WithIds_Login_Test() {
        loginMock();
        List<Movie> movieList = new ArrayList<>();
        mPresenter.getMyFavoriteMoviesWithId(movieList, true, reference);
        mTestScheduler.triggerActions();

        verify(mView).updateMovieFavoriteStatus(argThat(argument -> argument.size() == 0));
    }

    @Test
    public void getMyFavoriteMovies_Login_Error_Test() {
        login_Error_Mock(databaseError);

        mPresenter.loadMovies(MovieTabType.MY_FAVORITE, false, true, reference);
        mTestScheduler.triggerActions();

        verify(mView).showError(databaseError.getMessage());
    }

    @Test
    public void onFavoriteClick_Add_LogIn_Test() {
        Movie movie = createSingleMovie(1);
        movie.isFavorite = true;

        when(reference.child(any(String.class))).thenReturn(reference);
        doAnswer((Answer<Void>) invocation -> {
            DatabaseReference.CompletionListener listener
                    = (DatabaseReference.CompletionListener) invocation.getArguments()[1];
            listener.onComplete(null, reference);

            return null;
        }).when(reference).setValue(any(Movie.class), any(DatabaseReference.CompletionListener.class));
        mPresenter.onFavoriteClick(movie, true, reference);
        mTestScheduler.triggerActions();

        verify(mView).showSuccess();
    }

    @Test
    public void onFavoriteClick_Add_Error_LogIn_Test() {
        Movie movie = createSingleMovie(1);
        movie.isFavorite = true;

        when(reference.child(any(String.class))).thenReturn(reference);
        doAnswer((Answer<Void>) invocation -> {
            DatabaseReference.CompletionListener listener
                    = (DatabaseReference.CompletionListener) invocation.getArguments()[1];
            listener.onComplete(databaseError, reference);

            return null;
        }).when(reference).setValue(any(Movie.class), any(DatabaseReference.CompletionListener.class));
        mPresenter.onFavoriteClick(movie, true, reference);
        mTestScheduler.triggerActions();

        verify(mView).showError(databaseError.getMessage());
    }

    @Test
    public void onFavoriteClick_Remove_LogIn_Test() {
        Movie movie = createSingleMovie(1);
        when(reference.child(any(String.class))).thenReturn(reference);
        doAnswer((Answer<Void>) invocation -> {
            DatabaseReference.CompletionListener listener
                    = (DatabaseReference.CompletionListener) invocation.getArguments()[0];
            listener.onComplete(null, reference);

            return null;
        }).when(reference).removeValue(any(DatabaseReference.CompletionListener.class));
        mPresenter.onFavoriteClick(movie, true, reference);
        mTestScheduler.triggerActions();

        verify(mView).showSuccess();
    }

    @Test
    public void onFavoriteClick_Remove_Error_LogIn_Test() {
        Movie movie = createSingleMovie(1);
        when(reference.child(any(String.class))).thenReturn(reference);
        doAnswer((Answer<Void>) invocation -> {
            DatabaseReference.CompletionListener listener
                    = (DatabaseReference.CompletionListener) invocation.getArguments()[0];
            listener.onComplete(databaseError, reference);

            return null;
        }).when(reference).removeValue(any(DatabaseReference.CompletionListener.class));
        mPresenter.onFavoriteClick(movie, true, reference);
        mTestScheduler.triggerActions();

        verify(mView).showError(databaseError.getMessage());
    }

    private void loginMock() {
        doAnswer((Answer<Void>) invocation -> {
            ValueEventListener valueEventListener = (ValueEventListener) invocation.getArguments()[0];
            DataSnapshot dataSnapshot = mock(DataSnapshot.class);
            valueEventListener.onDataChange(dataSnapshot);

            return null;
        }).when(reference).addValueEventListener(any(ValueEventListener.class));
    }

    private void login_Error_Mock(DatabaseError databaseError) {
        doAnswer((Answer<Void>) invocation -> {
            ValueEventListener valueEventListener = (ValueEventListener) invocation.getArguments()[0];
            valueEventListener.onCancelled(databaseError);

            return null;
        }).when(reference).addValueEventListener(any(ValueEventListener.class));
    }
}
