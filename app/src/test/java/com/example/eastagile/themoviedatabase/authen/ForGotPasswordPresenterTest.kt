package com.example.eastagile.themoviedatabase.authen

import com.example.eastagile.themoviedatabase.TestUtils.EMAIL
import com.example.eastagile.themoviedatabase.TestUtils.FAKE_ERROR
import com.example.eastagile.themoviedatabase.authens.forgotpassword.ForgotPasswordContract
import com.example.eastagile.themoviedatabase.authens.forgotpassword.ForgotPasswordPresenter
import com.example.eastagile.themoviedatabase.util.AuthenUtils.ERROR_EMPTY
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class ForGotPasswordPresenterTest {
    private val mView = mock(ForgotPasswordContract.View::class.java)

    private val mAuth = mock(FirebaseAuth::class.java)

    private val authResult = mock(Task::class.java) as (Task<Void>)

    var mPresenter: ForgotPasswordContract.Presenter? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mPresenter = ForgotPasswordPresenter(mView, mAuth)
        `when`(mAuth.sendPasswordResetEmail(EMAIL))
                .thenReturn(authResult)
    }

    @Test
    fun sendSuccess() {
        `when`(authResult.isSuccessful).thenReturn(true)

        doAnswer {
            val listener = it.arguments[0] as OnCompleteListener<Void>
            listener.onComplete(authResult)
            authResult
        }.`when`(authResult).addOnCompleteListener(any<OnCompleteListener<Void>>())

        mPresenter?.sendPasswordResetEmail(EMAIL)

        verify(mView).showSuccess()
    }

    @Test
    fun sendError() {
        `when`(authResult.isSuccessful).thenReturn(false)
        `when`(authResult.exception).thenReturn(Exception(FAKE_ERROR))

        doAnswer {
            val listener = it.arguments[0] as OnCompleteListener<Void>
            listener.onComplete(authResult)
            authResult
        }.`when`(authResult).addOnCompleteListener(any<OnCompleteListener<Void>>())


        mPresenter?.sendPasswordResetEmail(EMAIL)

        verify(mView).showError(FAKE_ERROR)
    }

    @Test
    fun email_Empty_Test() {
        mPresenter?.sendPasswordResetEmail("")

        verify(mView).showError(ERROR_EMPTY)
    }

    @Test
    fun start() {
        mPresenter?.start(mView)
    }

    @Test
    fun stop() {
        mPresenter?.stop()
    }

}
