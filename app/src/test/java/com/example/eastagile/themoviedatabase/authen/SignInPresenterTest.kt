package com.example.eastagile.themoviedatabase.authen

import com.example.eastagile.themoviedatabase.TestUtils.*
import com.example.eastagile.themoviedatabase.authens.signin.SignInContract
import com.example.eastagile.themoviedatabase.authens.signin.SignInPresenter
import com.example.eastagile.themoviedatabase.repo.MovieRepo
import com.example.eastagile.themoviedatabase.util.AuthenUtils.ERROR_EMPTY
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class SignInPresenterTest {
    private val mView = mock(SignInContract.View::class.java)

    private val mAuth = mock(FirebaseAuth::class.java)

    private val authResult = mock(Task::class.java) as (Task<AuthResult>)

    private var mPresenter: SignInContract.Presenter? = null

    private val movieRepo = mock(MovieRepo::class.java)

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mPresenter = SignInPresenter(mView, mAuth, movieRepo)
        `when`(mAuth.signInWithEmailAndPassword(EMAIL, PASSWORD))
                .thenReturn(authResult)
    }

    @Test
    fun signInSuccess() {
        `when`(authResult.isSuccessful).thenReturn(true)

        doAnswer {
            val listener = it.arguments[0] as OnCompleteListener<AuthResult>
            listener.onComplete(authResult)
            authResult
        }.`when`(authResult).addOnCompleteListener(any<OnCompleteListener<AuthResult>>())

        // Result for local db data
        val mListEntity = createMovieEntity(10)

        `when`(movieRepo.moviesFavourite)
                .thenReturn(Flowable.just(mListEntity))
        mPresenter?.signIn(EMAIL, PASSWORD, null)

        verify(mView).showSuccess()
    }

    @Test
    fun signInError() {
        `when`(authResult.isSuccessful).thenReturn(false)
        `when`(authResult.exception).thenReturn(Exception(FAKE_ERROR))

        doAnswer {
            val listener = it.arguments[0] as OnCompleteListener<AuthResult>
            listener.onComplete(authResult)
            authResult
        }.`when`(authResult).addOnCompleteListener(any<OnCompleteListener<AuthResult>>())


        mPresenter?.signIn(EMAIL, PASSWORD, null)

        verify(mView).showError(FAKE_ERROR)
    }

    @Test
    fun email_Empty_Test() {
        mPresenter?.signIn("", PASSWORD, null)
        verify(mView).showError(ERROR_EMPTY)
    }

    @Test
    fun password_Empty_Test() {
        mPresenter?.signIn(EMAIL, "", null)
        verify(mView).showError(ERROR_EMPTY)
    }

    @Test
    fun start() {
        mPresenter?.start(mView)
    }

    @Test
    fun stop() {
        mPresenter?.stop()
    }

}
