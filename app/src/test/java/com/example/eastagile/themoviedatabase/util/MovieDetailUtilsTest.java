package com.example.eastagile.themoviedatabase.util;

import com.example.eastagile.themoviedatabase.TestUtils;
import com.example.eastagile.themoviedatabase.model.MovieReview;
import com.example.eastagile.themoviedatabase.model.MovieTrailer;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class MovieDetailUtilsTest {

    @Test
    public void convertTrailer_Empty_Test() {
        List<MovieTrailer> trailer = MovieDetailUtils.convertTrailers(null);
        assertTrue(trailer.isEmpty());

        trailer = MovieDetailUtils.convertTrailers(TestUtils.createTrailerResult(0));
        assertTrue(trailer.isEmpty());

        MovieTrailerResult trailerResult = TestUtils.createTrailerResult(0);
        trailerResult.results = null;
        trailer = MovieDetailUtils.convertTrailers(trailerResult);
        assertTrue(trailer.isEmpty());
    }

    @Test
    public void convertReview_Empty_Test() {
        List<MovieReview> trailer = MovieDetailUtils.convertReviews(null);
        assertTrue(trailer.isEmpty());

        trailer = MovieDetailUtils.convertReviews(TestUtils.createMovieReviewResult(0));
        assertTrue(trailer.isEmpty());

        MovieReviewResult reviewResult = TestUtils.createMovieReviewResult(0);
        reviewResult.results = null;
        trailer = MovieDetailUtils.convertReviews(reviewResult);
        assertTrue(trailer.isEmpty());
    }
}
