package com.example.eastagile.themoviedatabase.util;

import com.example.eastagile.themoviedatabase.TestUtils;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MovieUtilsTest {

    @Test
    public void convertMovieToEntity() {
        int numMovie = 2;
        Movie[] movies = new Movie[numMovie];
        for (int i = 0; i < numMovie; i++) {
            movies[i] = new Movie();
            movies[i].id = i;
        }

        List<MoviePosterEntity> entities = MovieUtils.convertMovieToEntity(movies);
        assertEquals(entities.size(), numMovie);
        assertEquals(movies[0].id, entities.get(0).id);
    }

    @Test
    public void convertMovieToEntityEmpty() {
        List<MoviePosterEntity> entities = MovieUtils.convertMovieToEntity();
        assertTrue(entities.isEmpty());
    }

    @Test
    public void convertDtoToMovie_Empty_Test() {
        List<Movie> movie = MovieUtils.convertDtoToMovie(null);
        assertTrue(movie.isEmpty());

        movie = MovieUtils.convertDtoToMovie(TestUtils.createMovieResult(0));
        assertTrue(movie.isEmpty());
    }
}
