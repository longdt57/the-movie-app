package com.example.eastagile.themoviedatabase.home;


import com.example.eastagile.themoviedatabase.home.fragment.MovieTabContract;
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabPresenter;
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabType;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;
import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepoImpl;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
import com.google.firebase.database.DatabaseReference;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;

import static com.example.eastagile.themoviedatabase.TestUtils.createMovieEntity;
import static com.example.eastagile.themoviedatabase.TestUtils.createMovieResult;
import static com.example.eastagile.themoviedatabase.TestUtils.createSingleMovie;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieTabPresenterTest {

    private static final String error = "error";

    // Result for local db data
    private final List<MoviePosterEntity> mListEntity = createMovieEntity(10);

    // Result for network api data
    private final MovieResult mResult = createMovieResult(10);
    @Mock
    private MovieRepoImpl mMovieRepo;
    @Mock
    private MovieTabContract.View mView;

    @Mock
    private DatabaseReference reference;
    private MovieTabPresenter mPresenter;
    private TestScheduler mTestScheduler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mTestScheduler = new TestScheduler();
        mPresenter = new MovieTabPresenter(mMovieRepo, mTestScheduler, mTestScheduler);
        mPresenter.start(mView);
    }

    @Test
    public void loadMoviePopularTest() {
        when(mMovieRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR))
                .thenReturn(Observable.just(mResult));
        mPresenter.loadMovies(MovieTabType.POPULAR, true, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showMovies(argThat(argument -> argument.size() == mResult.results.size()));
    }

    @Test
    public void loadMoviePopular_Error_Test() {
        when(mMovieRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR))
                .thenReturn(Observable.error(new Throwable(error)));
        mPresenter.loadMovies(MovieTabType.POPULAR, true, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showError(eq(error));
    }

    @Test
    public void loadMovieMostRateTest() {
        when(mMovieRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.RATE))
                .thenReturn(Observable.just(mResult));
        mPresenter.loadMovies(MovieTabType.MOST_RATE, true, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showMovies(argThat(argument -> argument.size() == mResult.results.size()));
    }

    @Test
    public void loadMoreMovieTest() {
        int currentPage = 2;
        int loadPage = currentPage + 1;
        when(mMovieRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR, loadPage))
                .thenReturn(Observable.just(mResult));
        mPresenter.loadMoreMovies(MovieTabType.POPULAR, currentPage);
        mTestScheduler.triggerActions();

        verify(mView).showMoreMovies(argThat(argument -> argument.size() == mResult.results.size()),
                eq(loadPage));
    }

    @Test
    public void loadMoreMovieError() {
        int currentPage = 2;
        when(mMovieRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR, currentPage + 1))
                .thenReturn(Observable.error(new Throwable(error)));
        mPresenter.loadMoreMovies(MovieTabType.POPULAR, currentPage);
        mTestScheduler.triggerActions();

        verify(mView).showError(eq(error));
    }


    @Test
    public void getMyFavoriteMoviesWithIdsTest() {
        // Fake list to avoid null pointer
        List<Movie> movieList = new ArrayList<>();
        when(mMovieRepo.getMoviesFavoriteById())
                .thenReturn(Flowable.just(mListEntity.subList(0, 0)));
        mPresenter.getMyFavoriteMoviesWithId(movieList, false, null);
        mTestScheduler.triggerActions();

        verify(mView).updateMovieFavoriteStatus(eq(movieList));
    }

    @Test
    public void getMyFavoriteMoviesWithIdsErrorTest() {
        // Fake list to avoid null pointer
        List<Movie> movieList = new ArrayList<>();
        when(mMovieRepo.getMoviesFavoriteById())
                .thenReturn(Flowable.error(new Throwable(error)));
        mPresenter.getMyFavoriteMoviesWithId(movieList, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showError(error);
    }

    @Test
    public void getMyFavoriteMoviesTest() {
        when(mMovieRepo.getMoviesFavourite())
                .thenReturn(Flowable.just(mListEntity));
        mPresenter.loadMovies(MovieTabType.MY_FAVORITE, false, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showMovies(argThat(argument -> argument.size() == mListEntity.size()));
    }


    @Test
    public void getMyFavoriteMoviesErrorTest() {
        when(mMovieRepo.getMoviesFavourite())
                .thenReturn(Flowable.error(new Throwable(error)));
        mPresenter.loadMovies(MovieTabType.MY_FAVORITE, false, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showError(error);
    }

    @Test
    public void onFavoriteClickAdd() {
        Movie movie = createSingleMovie(1);
        movie.isFavorite = true;
        when(mMovieRepo.addFavoriteMovie(any(List.class))).thenReturn(
                Completable.fromAction(() -> {
                })
        );
        mPresenter.onFavoriteClick(movie, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showSuccess();
    }

    @Test
    public void onFavoriteClickRemove() {
        Movie movie = createSingleMovie(1);
        when(mMovieRepo.removeFavoriteMovies(any(List.class))).thenReturn(
                Completable.fromAction(() -> {}));
        mPresenter.onFavoriteClick(movie, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showSuccess();
    }

    @Test
    public void onFavoriteClickError() {
        Movie movie = createSingleMovie(1);
        movie.isFavorite = true;
        when(mMovieRepo.addFavoriteMovie(any(List.class))).thenReturn(
                Completable.error(new Throwable(error)));
        mPresenter.onFavoriteClick(movie, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showError(error);
    }

    @Test
    public void start() {
        mPresenter.start(mView);
    }

    @Test
    public void stop() {
        mPresenter.stop();
    }

}
