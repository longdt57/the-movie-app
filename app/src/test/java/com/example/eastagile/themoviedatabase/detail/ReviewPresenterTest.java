package com.example.eastagile.themoviedatabase.detail;

import com.example.eastagile.themoviedatabase.detail.fragment.reviews.ReviewContract;
import com.example.eastagile.themoviedatabase.detail.fragment.reviews.ReviewPresenter;
import com.example.eastagile.themoviedatabase.model.MovieReview;
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;

import static com.example.eastagile.themoviedatabase.TestUtils.ERROR;
import static com.example.eastagile.themoviedatabase.TestUtils.FAKE_MOVIE_ID;
import static com.example.eastagile.themoviedatabase.TestUtils.createMovieReviewResult;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReviewPresenterTest {

    @Mock
    private MovieRepoImpl mMovieRepo;

    @Mock
    private ReviewContract.View mView;

    private ReviewPresenter mPresenter;

    private TestScheduler mTestScheduler;

    @Captor
    private ArgumentCaptor<List<MovieReview>> captor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = new TestScheduler();
        mPresenter = new ReviewPresenter(mMovieRepo, mView, mTestScheduler, mTestScheduler);
    }

    @Test
    public void loadReviewSuccess() {
        MovieReviewResult result = createMovieReviewResult(10);
        when(mMovieRepo.getMovieReviews(FAKE_MOVIE_ID))
                .thenReturn(Observable.just(result));

        mPresenter.loadReviews(FAKE_MOVIE_ID, true);
        mTestScheduler.triggerActions();

        List<MovieReview> expectedReviews = MovieDetailUtils.convertReviews(result);
        verifyShowingReviews(expectedReviews);
    }

    @Test
    public void loadReview_Ignore_Test() {
        mPresenter.mFirstLoad = false;
        mPresenter.loadReviews(FAKE_MOVIE_ID, false);

        verify(mMovieRepo, never()).getMovieReviews(anyString());
    }

    @Test
    public void loadReviewError() {
        when(mMovieRepo.getMovieReviews(any(String.class)))
                .thenReturn(Observable.error(new Throwable(ERROR)));

        mPresenter.loadReviews(FAKE_MOVIE_ID, true);
        mTestScheduler.triggerActions();

        verify(mView).showError(ERROR);
    }

    @Test
    public void starTest() {
        mPresenter.start(mView);
        verify(mView).setLoadingIndicator(false);
    }

    @Test
    public void stopTest() {
        mPresenter.stop();
    }

    private void verifyShowingReviews(List<MovieReview> expectedReviews) {
        verify(mView).showReviews(captor.capture());

        List<MovieReview> actualReviews = captor.getValue();

        assertEquals(expectedReviews.size(), actualReviews.size());

        for (int i = 0; i < expectedReviews.size(); i++) {
            assertEquals(expectedReviews.get(i).author, actualReviews.get(i).author);
            assertEquals(expectedReviews.get(i).content, actualReviews.get(i).content);
        }
    }

}
