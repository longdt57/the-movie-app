package com.example.eastagile.themoviedatabase.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MovieTest {

    @Test
    public void equal() {
        Movie a = new Movie();
        Movie b = new Movie();
        a.id = 1;
        b.id = 1;

        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
        assertEquals(a.describeContents(), b.describeContents());

    }

    @Test
    public void notEqual() {
        Movie a = new Movie();
        Movie b = new Movie();
        a.id = 1;
        b.id = 2;

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());

        assertNotEquals("abc", a);

    }

}
