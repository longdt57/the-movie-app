package com.example.eastagile.themoviedatabase.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UtilsTest {

    @Test
    public void string_Empty_Test() {
        assertTrue(Utils.isEmpty(null));
        assertTrue(Utils.isEmpty(""));
    }

    @Test
    public void convertDateSuccessTest() {
        String fakeDate = "2018-09-11";
        String out = Utils.convertReleaseDate(fakeDate);
        assertEquals(out, "September 2018");
    }

    @Test
    public void convertDateErrorTest() {
        String fakeDate = "09-11";
        String out = Utils.convertReleaseDate(fakeDate);
        assertEquals(out, "");
    }

}
