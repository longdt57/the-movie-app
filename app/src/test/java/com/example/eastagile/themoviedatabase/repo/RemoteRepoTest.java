//package com.example.eastagile.themoviedatabase.repo;
//
//import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepo;
//import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepoImpl;
//import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieDto;
//import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
//import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
//import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import java.util.List;
//
//import static org.junit.Assert.assertNotEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;
//
///**
// * This test is about checking API work well with internet
// */
//
//public class RemoteRepoTest {
//
//    private MovieRemoteRepo movieRemoteRepo;
//
//    @Before
//    public void setUp() {
//        movieRemoteRepo = new MovieRemoteRepoImpl();
//    }
//
//    @Test
//    public void getMoviesValidResultTest() {
//        MovieResult result = movieRemoteRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR).blockingFirst();
//
//        assertNotEquals(result.totalPages, 0);
//        assertNotEquals(result.results.size(), 0);
//
//    }
//
//    @Test
//    public void getMoviesByPopularValidResultTest() {
//        MovieResult result = movieRemoteRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR).blockingFirst();
//
//        // Make sure popular sort desc
//        assertNotNull(result);
//        List<MovieDto> list = result.results;
//        assertNotNull(list);
//
//        assertTrue(list.size() > 2);
//        assertTrue(list.get(0).popularity >= list.get(1).popularity);
//
//    }
//
//    @Test
//    public void getMoviesByWithPageTest() {
//        MovieResult result = movieRemoteRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR, 1).blockingFirst();
//
//        // Make sure popular sort desc
//        assertNotNull(result);
//        List<MovieDto> list = result.results;
//        assertNotNull(list);
//
//        assertTrue(list.size() > 2);
//        assertTrue(list.get(0).popularity >= list.get(1).popularity);
//
//    }
//
//    @Test
//    public void getMoviesByRateValidResultTest() {
//        MovieResult result = movieRemoteRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.RATE).blockingFirst();
//
//        // Make sure rate sort desc
//        assertNotNull(result);
//        List<MovieDto> list = result.results;
//        assertNotNull(list);
//
//        assertTrue(list.size() > 2);
//        assertTrue(list.get(0).voteAverage >= list.get(1).voteAverage);
//
//    }
//
//    @Test
//    public void getTrailerValidResultTest() {
//        MovieResult result = movieRemoteRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.RATE).blockingFirst();
//        String movieId = String.valueOf(result.results.get(0).id);
//        MovieTrailerResult trailerResult = movieRemoteRepo.getMovieTrailers(movieId).blockingFirst();
//
//        assertNotNull(trailerResult);
//
//    }
//
//    @Test
//    public void getReviewValidResultTest() {
//        MovieResult result = movieRemoteRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.RATE).blockingFirst();
//        String movieId = String.valueOf(result.results.get(0).id);
//        MovieReviewResult reviewResult = movieRemoteRepo.getMovieReviews(movieId).blockingFirst();
//
//        assertNotNull(reviewResult);
//    }
//
//}
