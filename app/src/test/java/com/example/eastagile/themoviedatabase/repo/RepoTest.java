package com.example.eastagile.themoviedatabase.repo;

import com.example.eastagile.themoviedatabase.TestUtils;
import com.example.eastagile.themoviedatabase.repo.local.MovieLocalRepo;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;
import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepo;
import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepoImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class RepoTest {

    private final String FAKE_MOVIE_ID = "123";
    private final List<MoviePosterEntity> fakeEntity = TestUtils.createMovieEntity(10);
    @Mock
    private MovieRemoteRepo remoteRepo;
    @Mock
    private MovieLocalRepo localRepo;
    private MovieRepo movieRepo;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        movieRepo = new MovieRepoImpl(localRepo, remoteRepo);

    }

    @Test
    public void getMoviesTest() {
        movieRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR);
        verify(remoteRepo).getMoviesBy(eq(MovieRemoteRepoImpl.MovieSortType.POPULAR));
    }

    @Test
    public void getMoviesWithPageTest() {
        int fakePage = 2;

        movieRepo.getMoviesBy(MovieRemoteRepoImpl.MovieSortType.POPULAR, fakePage);
        verify(remoteRepo).getMoviesBy(eq(MovieRemoteRepoImpl.MovieSortType.POPULAR), eq(2));
    }

    @Test
    public void getMovieTrailersTest() {

        movieRepo.getMovieTrailers(FAKE_MOVIE_ID);
        verify(remoteRepo).getMovieTrailers(eq(FAKE_MOVIE_ID));
    }

    @Test
    public void getMovieReviewsTest() {

        movieRepo.getMovieReviews(FAKE_MOVIE_ID);
        verify(remoteRepo).getMovieReviews(eq(FAKE_MOVIE_ID));

    }

    @Test
    public void getMoviesFavouriteTest() {

        movieRepo.getMoviesFavourite();
        verify(localRepo).getAllFavoriteMovies();
    }

    @Test
    public void deleteAllMoviesFavouriteTest() {

        movieRepo.deleteFavoriteAll();
        verify(localRepo).deleteAll();
    }

    @Test
    public void getMoviesFavouriteByIdTest() {
        int ids = 1;

        movieRepo.getMoviesFavoriteById(ids);
        verify(localRepo).getFavoriteMoviesByIds(eq(ids));
    }

    @Test
    public void addFavoriteMovieTest() {
        movieRepo.addFavoriteMovie(fakeEntity);
        verify(localRepo).addFavoriteMovies(eq(fakeEntity));
    }

    @Test
    public void removeFavoriteMoviesTest() {
        movieRepo.removeFavoriteMovies(fakeEntity);
        verify(localRepo).removeFavoriteMovies(eq(fakeEntity));
    }

}
