package com.example.eastagile.themoviedatabase.detail;

import com.example.eastagile.themoviedatabase.detail.fragment.trailer.TrailerContract;
import com.example.eastagile.themoviedatabase.detail.fragment.trailer.TrailerPresenter;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.model.MovieTrailer;
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;

import static com.example.eastagile.themoviedatabase.TestUtils.ERROR;
import static com.example.eastagile.themoviedatabase.TestUtils.FAKE_MOVIE_ID;
import static com.example.eastagile.themoviedatabase.TestUtils.createSingleMovie;
import static com.example.eastagile.themoviedatabase.TestUtils.createTrailerResult;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TrailerPresenterTest {
    @Mock
    private MovieRepoImpl mMovieRepo;
    @Mock
    private TrailerContract.View mView;
    private TrailerContract.Presenter mPresenter;
    private TestScheduler mTestScheduler;
    @Captor
    private ArgumentCaptor<List<MovieTrailer>> captor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = new TestScheduler();
        mPresenter = new TrailerPresenter(mMovieRepo, mView, mTestScheduler, mTestScheduler);
    }

    @Test
    public void loadTrailerSuccessTest() {
        MovieTrailerResult result = createTrailerResult(10);

        when(mMovieRepo.getMovieTrailers(FAKE_MOVIE_ID))
                .thenReturn(Observable.just(result));

        mPresenter.loadTrailers(FAKE_MOVIE_ID, true);
        mTestScheduler.triggerActions();

        verify(mView).showTrailers(captor.capture());

        List<MovieTrailer> source = MovieDetailUtils.convertTrailers(result);
        List<MovieTrailer> des = captor.getValue();

        assertEquals(source.size(), des.size());

        for (int i = 0; i < source.size(); i++) {
            assertEquals(source.get(i).title, des.get(i).title);
        }

    }

    @Test
    public void loadTrailerErrorTest() {
        when(mMovieRepo.getMovieTrailers(FAKE_MOVIE_ID))
                .thenReturn(Observable.error(new Throwable(ERROR)));

        mPresenter.loadTrailers(FAKE_MOVIE_ID, true);
        mTestScheduler.triggerActions();

        verify(mView).showError(ERROR);

    }

    @Test
    public void start() {
        mPresenter.start(mView);
    }

    @Test
    public void stop() {
        mPresenter.stop();
    }

    @Test
    public void onFavoriteClickAdd() {
        Movie movie = createSingleMovie(1);
        movie.isFavorite = true;
        when(mMovieRepo.addFavoriteMovie(any(List.class))).thenReturn(
                Completable.fromAction(() -> {
                })
        );

        mPresenter.onFavoriteClick(movie, false, null);
        mTestScheduler.triggerActions();

        verify(mView).showSuccess();
    }

    @Test
    public void onFavoriteClickRemove() {
        Movie movie = new Movie();
        movie.id = 1;
        movie.isFavorite = false;
        when(mMovieRepo.removeFavoriteMovies(any(List.class))).thenReturn(
                Completable.fromAction(() -> {
                })
        );

        mPresenter.onFavoriteClick(movie, false, null);
        mTestScheduler.triggerActions();
        verify(mView).showSuccess();
    }

    @Test
    public void onFavoriteClickError() {
        Movie movie = new Movie();
        movie.id = 1;
        movie.isFavorite = true;
        when(mMovieRepo.addFavoriteMovie(any(List.class))).thenReturn(
                Completable.error(new Throwable(ERROR))
        );


        mPresenter.onFavoriteClick(movie, false, null);
        mTestScheduler.triggerActions();
        verify(mView).showError(ERROR);
    }

    @Test
    public void onFavorite_Remove_Error() {
        Movie movie = new Movie();
        movie.id = 1;
        movie.isFavorite = false;
        when(mMovieRepo.removeFavoriteMovies(any(List.class))).thenReturn(
                Completable.error(new Throwable(ERROR))
        );

        mPresenter.onFavoriteClick(movie, false, null);
        mTestScheduler.triggerActions();
        verify(mView).showError(ERROR);
    }

}
