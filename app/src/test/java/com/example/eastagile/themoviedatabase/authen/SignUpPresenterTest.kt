package com.example.eastagile.themoviedatabase.authen

import com.example.eastagile.themoviedatabase.TestUtils.*
import com.example.eastagile.themoviedatabase.authens.signup.SignUpContract
import com.example.eastagile.themoviedatabase.authens.signup.SignUpPresenter
import com.example.eastagile.themoviedatabase.util.AuthenUtils.ERROR_EMPTY
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class SignUpPresenterTest {
    private val mView = mock(SignUpContract.View::class.java)

    private val mAuth = mock(FirebaseAuth::class.java)

    private val authResult = mock(Task::class.java) as (Task<AuthResult>)

    var mPresenter: SignUpContract.Presenter? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mPresenter = SignUpPresenter(mView, mAuth)
        `when`(mAuth.createUserWithEmailAndPassword(EMAIL, PASSWORD))
                .thenReturn(authResult)
    }

    @Test
    fun signUpSuccess() {
        `when`(authResult.isSuccessful).thenReturn(true)

        doAnswer {
            val listener = it.arguments[0] as OnCompleteListener<AuthResult>
            listener.onComplete(authResult)
            authResult
        }.`when`(authResult).addOnCompleteListener(any<OnCompleteListener<AuthResult>>())

        mPresenter?.signUp(EMAIL, PASSWORD)

        verify(mView).showSuccess()
    }

    @Test
    fun signUpError() {
        `when`(authResult.isSuccessful).thenReturn(false)
        `when`(authResult.exception).thenReturn(Exception(FAKE_ERROR))

        doAnswer {
            val listener = it.arguments[0] as OnCompleteListener<AuthResult>
            listener.onComplete(authResult)
            authResult
        }.`when`(authResult).addOnCompleteListener(any<OnCompleteListener<AuthResult>>())


        mPresenter?.signUp(EMAIL, PASSWORD)

        verify(mView).showError(FAKE_ERROR)
    }

    @Test
    fun email_Empty_Test() {
        mPresenter?.signUp("", PASSWORD)
        verify(mView).showError(ERROR_EMPTY)
    }

    @Test
    fun password_Empty_Test() {
        mPresenter?.signUp(EMAIL, "")
        verify(mView).showError(ERROR_EMPTY)
    }

    @Test
    fun start() {
        mPresenter?.start(mView)
    }

    @Test
    fun stop() {
        mPresenter?.stop()
    }

}
