package com.example.eastagile.themoviedatabase;

import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieDto;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewDto;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerDto;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {

    public static final String FAKE_MOVIE_ID = "123";

    public static final String ERROR = "error";

    public static final String EMAIL = "abc@gmail.com";
    public static final String PASSWORD = "abc123";
    public static final String FAKE_ERROR = "ERROR";

    public static List<MoviePosterEntity> createMovieEntity(int size) {
        List<MoviePosterEntity> mListEntity = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            MoviePosterEntity entity = new MoviePosterEntity();
            entity.id = i;
            mListEntity.add(entity);
        }

        return mListEntity;
    }

    public static MovieResult createMovieResult(int size) {
        // Init mResult
        List<MovieDto> listDto = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            MovieDto movieDto = new MovieDto();
            movieDto.id = i;
            listDto.add(movieDto);
        }

        MovieResult mResult = new MovieResult();
        mResult.page = 1;
        mResult.results = listDto;
        mResult.totalResults = size;
        mResult.totalPages = 1;

        return mResult;
    }

    public static MovieTrailerResult createTrailerResult(int size) {
        List<MovieTrailerDto> listDto = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            MovieTrailerDto movieDto = new MovieTrailerDto();
            movieDto.id = String.valueOf(i);
            listDto.add(movieDto);
        }

        MovieTrailerResult result = new MovieTrailerResult();
        result.id = 1;
        result.results = listDto;

        return result;
    }

    public static Movie createSingleMovie(int id) {
        return new Movie(id,
                "Venom",
                "https://image.tmdb.org/t/p/w500/2uNW4WbgBXL25BAbXGLnLqX71Sw.jpg",
                8.0f,
                "2018-10-03",
                "When Eddie Brock acquires the powers of a symbiote, he will have to release his alter-ego \"Venom\" to save his life.",
                false);
    }

    public static MovieReviewResult createMovieReviewResult(int size) {
        List<MovieReviewDto> listDto = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            MovieReviewDto movieDto = new MovieReviewDto();
            movieDto.id = String.valueOf(i);
            listDto.add(movieDto);
        }

        MovieReviewResult reviewResult = new MovieReviewResult();
        reviewResult.id = 1;
        reviewResult.page = 1;
        reviewResult.results = listDto;

        return reviewResult;
    }
}
