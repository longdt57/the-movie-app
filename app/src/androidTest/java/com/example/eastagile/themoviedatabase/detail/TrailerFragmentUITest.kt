package com.example.eastagile.themoviedatabase.detail

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import com.example.eastagile.themoviedatabase.AndroidTestUtils
import com.example.eastagile.themoviedatabase.AndroidTestUtils.makeListTrailer
import com.example.eastagile.themoviedatabase.AndroidTestUtils.makeSingleMovie
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.base.TestActivity
import com.example.eastagile.themoviedatabase.detail.fragment.trailer.TrailerFragment
import com.example.eastagile.themoviedatabase.detail.fragment.trailer.TrailerPresenter
import com.example.eastagile.themoviedatabase.model.Movie
import com.example.eastagile.themoviedatabase.util.Utils
import com.example.eastagile.themoviedatabase.util.Utils.convertReleaseDate
import com.google.firebase.auth.FirebaseAuth
import org.hamcrest.Matchers.*
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class TrailerFragmentUITest {

    @get:Rule
    val activityRule = ActivityTestRule(TestActivity::class.java)

    private lateinit var fragment: TrailerFragment
    private lateinit var movie: Movie

    @Before
    fun setUp() {
        movie = makeSingleMovie(1)
        fragment = TrailerFragment.newInstance(movie)
        fragment.mPresenter = mock(TrailerPresenter::class.java)
        activityRule.activity.supportFragmentManager.beginTransaction().replace(R.id.test_fragment, fragment).commit()
    }

    @Test
    fun presenter_Start_Test() {
        activityRule.runOnUiThread {}
        verify(fragment.mPresenter).start(eq(fragment))
    }

    @Test
    fun detailHeaderInformation_Test() {
        onView(allOf(withId(R.id.tv_title), isDescendantOfA(withId(R.id.movie_trailer_header))))
                .check(matches(withText(movie.title.toUpperCase())))
        onView(allOf(withId(R.id.tv_release_date), isDescendantOfA(withId(R.id.movie_trailer_header))))
                .check(matches(withText(convertReleaseDate(movie.releaseDate))))
        onView(allOf(withId(R.id.tv_content), isDescendantOfA(withId(R.id.movie_trailer_header))))
                .check(matches(withText(movie.overview)))

        // click on favorite icon
        val numTrailer = 10
        val listTrailer = makeListTrailer(numTrailer)
        activityRule.runOnUiThread { fragment.showTrailers(listTrailer) }
        onView(withId(R.id.rcv_content))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))

        onView(withId(R.id.imv_trailer_favorite)).perform(closeSoftKeyboard(), click())
        verify(fragment.mPresenter).onFavoriteClick(eq(movie),
                eq(FirebaseAuth.getInstance().currentUser != null),
                eq(Utils.getFavoriteDatabaseReference()))
    }

    @Test
    fun showTrailer_Test() {
        val numTrailer = 10
        val listTrailer = makeListTrailer(numTrailer)
        activityRule.runOnUiThread { fragment.showTrailers(listTrailer) }
        assertTrue(fragment.mAdapter.list.size == numTrailer)
    }

    @Test
    fun trailer_Item_Click_Test() {
        val numTrailer = 10
        val itemPosClick = 9
        val listTrailer = makeListTrailer(numTrailer)
        activityRule.runOnUiThread { fragment.showTrailers(listTrailer) }

        // click on item
        onView(withId(R.id.rcv_content))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemPosClick))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(itemPosClick, click()))
    }

    @Test
    fun show_Error_Test() {
        val fakeMessage = "error"
        activityRule.runOnUiThread { fragment.showError(fakeMessage) }

        onView(withText(fakeMessage)).check(matches(isDisplayed()))
    }

    @Test
    fun showSuccess_Test() {
        activityRule.runOnUiThread {
            fragment.showSuccess()
        }

        onView(withText(R.string.success)).inRoot(withDecorView(not(
                `is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
    }

    @Test
    fun indicator_Show_Test() {
        val showPercentage = 85
        activityRule.runOnUiThread {
            fragment.setLoadingIndicator(true)
        }

        onView(AndroidTestUtils.withIndex(withId(R.id.refresh_layout), 0)).check(matches(isDisplayingAtLeast(showPercentage)))
    }

    @Test
    fun updateMovie_Test() {
        val fakeTitle = "fake Title"
        val newMovie = makeSingleMovie(1)
        newMovie.title = fakeTitle
        activityRule.runOnUiThread { fragment.updateMovie(newMovie) }
        onView(allOf(withId(R.id.tv_title), isDescendantOfA(withId(R.id.movie_trailer_header))))
                .check(matches(withText(fakeTitle.toUpperCase())))
    }
}
