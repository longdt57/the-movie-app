package com.example.eastagile.themoviedatabase

import android.app.Activity
import android.content.pm.ActivityInfo
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.rule.ActivityTestRule
import android.view.View
import com.example.eastagile.themoviedatabase.model.Movie
import com.example.eastagile.themoviedatabase.model.MovieReview
import com.example.eastagile.themoviedatabase.model.MovieTrailer
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterDao
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import java.util.*

object AndroidTestUtils {

    // Use for swipe layout
    fun withCustomConstraints(action: ViewAction, constraints: Matcher<View>): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return constraints
            }

            override fun getDescription(): String {
                return action.description
            }

            override fun perform(uiController: UiController, view: View) {
                action.perform(uiController, view)
            }
        }
    }

    // User for recycler click children view
    fun clickChildViewWithId(id: Int): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View>? {
                return null
            }

            override fun getDescription(): String {
                return "Click on a child view with specified id."
            }

            override fun perform(uiController: UiController, view: View) {
                val v = view.findViewById<View>(id)
                v.performClick()
            }
        }
    }

    // Find view index i
    fun <T> withIndex(matcher: Matcher<T>, i: Int): Matcher<T> {
        return object : BaseMatcher<T>() {
            private var resultIndex = -1
            override fun matches(item: Any): Boolean {
                if (matcher.matches(item)) {
                    resultIndex++
                    return resultIndex == i
                }
                return false
            }

            override fun describeTo(description: Description) {}
        }
    }

    fun makeMovieEntity(num: Int, dao: MoviePosterDao): List<MoviePosterEntity> {
        val result = ArrayList<MoviePosterEntity>()

        for (i in 0 until num) {
            val entity = MoviePosterEntity()
            entity.id = i
            entity.title = "Hey $i"
            result.add(entity)
        }
        dao.insertMovies(result)

        return result
    }

    fun makeSingleMovie(id: Int): Movie {
        return Movie(id,
                "Venom",
                "https://image.tmdb.org/t/p/w500/2uNW4WbgBXL25BAbXGLnLqX71Sw.jpg",
                8.0f,
                "2018-10-03",
                "When Eddie Brock acquires the powers of a symbiote, he will have to release his alter-ego \"Venom\" to save his life.",
                true)
    }

    fun makeListMovie(size: Int): MutableList<Movie> {
        val list = mutableListOf<Movie>()
        for (i in 0 until size) {
            list.add(makeSingleMovie(i))
        }

        return list
    }

    fun makeListTrailer(size: Int): MutableList<MovieTrailer> {
        val list = mutableListOf<MovieTrailer>()

        for (i in 0 until size) {
            val trailer = MovieTrailer()
            trailer.title = "title $i"
            trailer.imagePath = "https://image.tmdb.org/t/p/w500/2uNW4WbgBXL25BAbXGLnLqX71Sw.jpg"

            list.add(trailer)
        }

        return list
    }

    fun makeListReview(size: Int): MutableList<MovieReview> {
        val list = mutableListOf<MovieReview>()

        for (i in 0 until size) {
            val item = MovieReview()
            item.author = "Author $i"
            item.content = "Content $i"
            list.add(item)
        }

        return list
    }

    fun setOrientationLanscape(activityTestRule: ActivityTestRule<out Activity>) {
        activityTestRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }
}
