package com.example.eastagile.themoviedatabase.utils

import android.content.res.Resources
import android.support.test.InstrumentationRegistry
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.util.Utils
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class UtilsTest {
    
    @Test(expected = Resources.NotFoundException::class)
    fun loadImageEmptyTest() {
        val imvDes = mock<ImageView>(ImageView::class.java)
        Utils.loadImage("", imvDes, null, InstrumentationRegistry.getContext())

        verify(imvDes).setImageDrawable(eq(ContextCompat.getDrawable(InstrumentationRegistry.getContext(),
                R.drawable.ic_no_image)))
    }

    @Test(expected = Resources.NotFoundException::class)
    fun loadImageErrorTest() {
        val errorUrl = "errorURL"
        val imvLoading = mock<ImageView>(ImageView::class.java)
        Utils.loadImage(errorUrl, null, imvLoading, InstrumentationRegistry.getContext())

        verify(imvLoading).setImageDrawable(eq(ContextCompat.getDrawable(InstrumentationRegistry.getContext(),
                R.drawable.ic_no_image)))
    }
}
