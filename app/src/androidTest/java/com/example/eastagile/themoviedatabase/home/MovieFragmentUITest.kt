package com.example.eastagile.themoviedatabase.home

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeDown
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.example.eastagile.themoviedatabase.AndroidTestUtils
import com.example.eastagile.themoviedatabase.AndroidTestUtils.clickChildViewWithId
import com.example.eastagile.themoviedatabase.AndroidTestUtils.makeListMovie
import com.example.eastagile.themoviedatabase.AndroidTestUtils.makeSingleMovie
import com.example.eastagile.themoviedatabase.AndroidTestUtils.withCustomConstraints
import com.example.eastagile.themoviedatabase.AndroidTestUtils.withIndex
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.base.TestActivity
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabFragment
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabPresenter
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabType
import com.example.eastagile.themoviedatabase.util.Utils
import com.google.firebase.auth.FirebaseAuth
import org.hamcrest.Matchers.*
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*


@RunWith(AndroidJUnit4::class)

class MovieFragmentUITest {

    @get:Rule
    val activityRule = ActivityTestRule(TestActivity::class.java)

    private val numberItemPerPage = 20
    private lateinit var fragment: MovieTabFragment

    @Before
    fun setUp() {
        fragment = MovieTabFragment.newInstance(MovieTabType.POPULAR)
        fragment.mPresenter = mock(MovieTabPresenter::class.java)
        fragment.mListener = mock(MovieTabFragment.PosterClickListener::class.java)
        activityRule.activity.supportFragmentManager.beginTransaction().replace(R.id.test_fragment, fragment).commit()
    }

    @Test
    fun presenter_Start_Test() {
        activityRule.runOnUiThread {}
        verify(fragment.mPresenter).start(eq(fragment))
    }

    @Test
    fun recycler_Item_Click_Test() {
        val listMovie = makeListMovie(numberItemPerPage)
        activityRule.runOnUiThread { fragment.showMovies(listMovie) }

        val itemPosClick = numberItemPerPage - 1
        val interaction = onView(allOf(withId(R.id.rcv_content), isDisplayed()))

        // click on favorite icon
        interaction.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0,
                clickChildViewWithId(R.id.imv_poster_favourite)))
        verify(fragment.mPresenter).onFavoriteClick(eq(listMovie[0]),
                eq(FirebaseAuth.getInstance().currentUser != null),
                eq(Utils.getFavoriteDatabaseReference()))

        // click on item
        interaction.perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemPosClick))
        interaction.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(itemPosClick, click()))
        verify(fragment.mListener).onPosterClick(eq(listMovie[itemPosClick]))
    }

    @Test
    fun recycler_Item_Click_Log_In_Test() {
        val listMovie = makeListMovie(numberItemPerPage)
        activityRule.runOnUiThread { fragment.showMovies(listMovie) }

        val itemPosClick = numberItemPerPage - 1
        val interaction = onView(allOf(withId(R.id.rcv_content), isDisplayed()))

        // click on favorite icon
        interaction.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0,
                clickChildViewWithId(R.id.imv_poster_favourite)))
        verify(fragment.mPresenter).onFavoriteClick(eq(listMovie[0]),
                eq(FirebaseAuth.getInstance().currentUser != null),
                any())

        // click on item
        interaction.perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemPosClick))
        interaction.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(itemPosClick, click()))
        verify(fragment.mListener).onPosterClick(eq(listMovie[itemPosClick]))
    }


    @Test
    fun showMovies_Test() {
        activityRule.runOnUiThread { fragment.showMovies(makeListMovie(numberItemPerPage)) }
        assertTrue(fragment.mAdapter.list.size == numberItemPerPage)
    }

    @Test
    fun showMoreMovies_Test() {
        val movieMoreSize = 10
        val nextPage = 2
        activityRule.runOnUiThread { fragment.showMovies(AndroidTestUtils.makeListMovie(numberItemPerPage)) }
        activityRule.runOnUiThread { fragment.showMoreMovies(AndroidTestUtils.makeListMovie(movieMoreSize), nextPage) }

        assertTrue(fragment.mAdapter.list.size == numberItemPerPage + movieMoreSize)
    }

    @Test
    fun onFavorite_Click_Test() {
        val movie = makeSingleMovie(1)
        activityRule.runOnUiThread { fragment.onFavoriteClick(movie, Utils.getFavoriteDatabaseReference()) }

        verify(fragment.mPresenter).onFavoriteClick(eq(movie),
                eq(FirebaseAuth.getInstance().currentUser != null),
                eq(Utils.getFavoriteDatabaseReference()))
    }

    @Test
    fun show_Error_Test() {
        val error = "Error"
        activityRule.runOnUiThread { fragment.showError(error) }

        onView(withText(error)).check(matches(isDisplayed()))
    }

    @Test
    fun show_Success_Test() {
        activityRule.runOnUiThread {
            fragment.showSuccess()
        }

        onView(withText(R.string.success)).inRoot(withDecorView(not(`is`(activityRule.activity.window
                .decorView)))).check(matches(isDisplayed()))
    }

    @Test
    fun indicator_Show_Test() {
        activityRule.runOnUiThread {
            fragment.setLoadingIndicator(true)
        }

        onView(withIndex(withId(R.id.refresh_layout), 0)).check(matches(isDisplayingAtLeast(85)))
    }

    @Test
    fun swipeRefresh_Test() {
        onView(withIndex(withId(R.id.refresh_layout), 0)).perform(withCustomConstraints(swipeDown(),
                isDisplayingAtLeast(85)))

        verify(fragment.mPresenter, times(2)).loadMovies(eq(MovieTabType.POPULAR), eq(true),
                eq(FirebaseAuth.getInstance().currentUser != null), eq(Utils.getFavoriteDatabaseReference()))
    }

}
