package com.example.eastagile.themoviedatabase.authen

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.signin.SignInActivity
import com.example.eastagile.themoviedatabase.authens.signup.SignUpActivity
import com.example.eastagile.themoviedatabase.authens.signup.SignUpPresenter
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.ERROR
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.FAKE_EMAIL
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.FAKE_PASSWORD
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class SignUpUITest {

    @get:Rule
    var activityRule = ActivityTestRule(SignUpActivity::class.java)

    @Before
    fun setUp() {
        activityRule.activity.mPresenter = mock(SignUpPresenter::class.java)
    }

    @Test
    fun btnSignUp_InvalidValue_Test() {
        onView(withId(R.id.btn_sign_up)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.sign_up)))
                .perform(closeSoftKeyboard(), click())

        verify(activityRule.activity.mPresenter).signUp(eq(""), eq(""))
    }

    @Test
    fun btnSignUp_ValidValue_Test() {
        onView(withId(R.id.edt_email)).perform(typeText(FAKE_EMAIL), closeSoftKeyboard())
        onView(withId(R.id.edt_password)).perform(typeText(FAKE_PASSWORD), closeSoftKeyboard())

        onView(withId(R.id.btn_sign_up)).perform(closeSoftKeyboard(), click())
        verify(activityRule.activity.mPresenter).signUp(FAKE_EMAIL, FAKE_PASSWORD)
    }

    @Test
    fun btnCancel_Test() {
        onView(withId(R.id.btn_cancel)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.cancel)))
                .perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
    }

    @Test
    fun tvSignIn_Test() {
        Intents.init()
        onView(withId(R.id.tv_sign_in)).check(matches(isDisplayed())).perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
        Intents.intended(hasComponent(SignInActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun showFail_Test() {
        activityRule.runOnUiThread { activityRule.activity.showError(ERROR) }

        onView(withId(R.id.tv_notify)).check(matches(withText(ERROR)))
    }

    @Test
    fun showSuccess_Test() {
        activityRule.runOnUiThread {
            activityRule.activity.showSuccess()
        }

        assertTrue(activityRule.activity.isFinishing)
    }
}
