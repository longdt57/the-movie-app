package com.example.eastagile.themoviedatabase.repo;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.eastagile.themoviedatabase.AndroidTestUtils;
import com.example.eastagile.themoviedatabase.repo.local.MovieDatabase;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterDao;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class MovieLocalRepoTest {

    private MoviePosterDao moviePosterDao;
    private MovieDatabase database;
    private static final int MAX_RECORD = 10;

    @Before
    public void setUp() {
        database = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getContext(), MovieDatabase.class)
                .allowMainThreadQueries()
                .build();
        moviePosterDao = database.moviePosterDao();
    }

    @After
    public void tearDown() {
        database.close();
    }

    @Test
    public void getAllMovie() {
        assertEquals(moviePosterDao.getAllMovies().blockingFirst().size(), 0);
    }

    @Test
    public void insertAndGetAllTest() {
        List<MoviePosterEntity> listIn = AndroidTestUtils.INSTANCE.makeMovieEntity(MAX_RECORD, moviePosterDao);
        List<MoviePosterEntity> listOut = moviePosterDao.getAllMovies().blockingFirst();

        assertEquals(listOut.size(), listIn.size());
        assertEquals(listOut.get(0).id, listIn.get(0).id);
        assertEquals(listOut.get(0).title, listIn.get(0).title);
    }

    @Test
    public void deleteTest() {
        List<MoviePosterEntity> listIn = AndroidTestUtils.INSTANCE.makeMovieEntity(MAX_RECORD, moviePosterDao);
        moviePosterDao.deleteMovies(listIn.subList(MAX_RECORD - 2, MAX_RECORD));
        List<MoviePosterEntity> listOut = moviePosterDao.getAllMovies().blockingFirst();

        assertEquals(listOut.size(), listIn.size() - 2);
        assertEquals(listIn.get(0).id, listOut.get(0).id);
    }

    @Test
    public void getMoviesByIdTest() {
        AndroidTestUtils.INSTANCE.makeMovieEntity(MAX_RECORD, moviePosterDao);
        List<MoviePosterEntity> listOut = moviePosterDao.getMovies(0).blockingFirst();
        assertEquals(listOut.size(), 1);
        assertEquals(listOut.get(0).id, 0);

        listOut = moviePosterDao.getMovies(MAX_RECORD - 2, MAX_RECORD - 3, MAX_RECORD - 4)
                .blockingFirst();
        assertEquals(listOut.size(), 3);
        listOut = moviePosterDao.getMovies(2, 3, 4, MAX_RECORD).blockingFirst();
        assertEquals(listOut.size(), 3);
    }

    @Test
    public void deleteAllTest() {
        AndroidTestUtils.INSTANCE.makeMovieEntity(MAX_RECORD, moviePosterDao);
        moviePosterDao.deleteAll();
        assertEquals(moviePosterDao.getAllMovies().blockingFirst().size(), 0);
    }
}
