package com.example.eastagile.themoviedatabase.home

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import com.example.eastagile.themoviedatabase.AndroidTestUtils
import com.example.eastagile.themoviedatabase.AndroidTestUtils.makeSingleMovie
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.signin.SignInActivity
import com.example.eastagile.themoviedatabase.authens.signup.SignUpActivity
import com.example.eastagile.themoviedatabase.authens.splash.SplashActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import org.hamcrest.Matchers.allOf
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.mock

class MovieActivityUITest {

    @get:Rule
    val activityRule = IntentsTestRule(MovieActivity::class.java)

    @Test
    fun toolbar_Show_Test() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_toolbar_title)).check(matches(withText("")))
        onView(withId(R.id.item_user)).check(matches(isDisplayed()))
    }

    @Test
    fun tabLayout_Show_Test() {
        val tabTitle = listOf<String>(
                activityRule.activity.getString(R.string.title_popular),
                activityRule.activity.getString(R.string.title_most_rated),
                activityRule.activity.getString(R.string.title_my_fav))

        onView(withId(R.id.tab_layout)).perform(closeSoftKeyboard(), click())
        onView(withId(R.id.tab_layout)).check(matches(isDisplayed()))

        // Test tab item
        for (text in tabTitle) {
            val matcher = allOf(withText(text.toUpperCase()), isDescendantOfA(withId(R.id.tab_layout)))
            onView(matcher).perform(closeSoftKeyboard(), click())
                    .check(matches(isDisplayed()))
                    .check(matches(isSelected()))
        }
    }

    @Test
    fun orientation_Test() {
        onView(allOf(withText(R.string.title_most_rated), isDescendantOfA(withId(R.id.tab_layout)))).perform(closeSoftKeyboard(), click())
        AndroidTestUtils.setOrientationLanscape(activityRule)
        onView(allOf(withText(R.string.title_most_rated), isDescendantOfA(withId(R.id.tab_layout)))).check(matches(isSelected()))
    }

    @Test
    fun menuItem_Sign_In_Test() {
        onView(withId(R.id.item_user)).check(matches(isDisplayed()))
        onView(withId(R.id.item_user)).perform(closeSoftKeyboard(), click())
        onView(withText(R.string.title_menu_item_sign_in)).check(matches(isDisplayed())).perform(closeSoftKeyboard(), click())
        Intents.intended(hasComponent(SignInActivity::class.java.name))
    }

    @Test
    fun menuItem_Sign_Up_Test() {
        onView(withId(R.id.item_user)).check(matches(isDisplayed()))
        onView(withId(R.id.item_user)).perform(closeSoftKeyboard(), click())
        onView(withText(R.string.title_menu_item_sign_up)).check(matches(isDisplayed())).perform(closeSoftKeyboard(), click())
        Intents.intended(IntentMatchers.hasComponent(SignUpActivity::class.java.name))
    }

    @Test
    fun menuItem_Sign_Out_Test() {
        activityRule.activity.mAuth = mock(FirebaseAuth::class.java)
        doReturn(mock(FirebaseUser::class.java)).`when`(activityRule.activity.mAuth).currentUser
        activityRule.runOnUiThread { activityRule.activity.invalidateOptionsMenu() }

        onView(withId(R.id.item_user)).check(matches(isDisplayed()))
        onView(withId(R.id.item_user)).perform(closeSoftKeyboard(), click())
        onView(withText(R.string.title_menu_item_sign_out)).check(matches(isDisplayed())).perform(closeSoftKeyboard(), click())
        Intents.intended(IntentMatchers.hasComponent(SplashActivity::class.java.name))
    }

    @Test
    fun onPosterClick_Test() {
        val movie = makeSingleMovie(1)
        activityRule.runOnUiThread { activityRule.activity.onPosterClick(movie) }

        assertNotNull(onView(allOf(withText(movie.title.toUpperCase()), isDisplayed())))
    }
}
