package com.example.eastagile.themoviedatabase.detail

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.example.eastagile.themoviedatabase.AndroidTestUtils
import com.example.eastagile.themoviedatabase.AndroidTestUtils.setOrientationLanscape
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailActivityUITest {

    @get:Rule
    val activityRule = ActivityTestRule(DetailActivity::class.java, false, false)

    private lateinit var fragment: DetailFragment
    val movie = AndroidTestUtils.makeSingleMovie(1)

    @Before
    fun setUp() {
        val bundle = Bundle().apply {
            putParcelable(MovieDetailUtils.MOVIE_OBJECT, movie)
        }
        val intent = Intent()
        intent.putExtras(bundle)
        activityRule.launchActivity(intent)
        fragment = activityRule.activity.supportFragmentManager.findFragmentById(R.id.fl_detail) as DetailFragment
    }

    @Test
    fun toolbar_Show_Test() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_toolbar_title)).check(matches(withText(movie.title.toUpperCase())))
        onView(withId(R.id.item_user)).check(matches(isDisplayed()))
    }

    @Test
    fun tabLayout_Show_Test() {
        onView(withId(R.id.tab_layout)).perform(closeSoftKeyboard(), click())
        onView(withId(R.id.tab_layout)).check(matches(isDisplayed()))

        val tabTitle = listOf<String>(
                activityRule.activity.resources.getString(R.string.title_details),
                activityRule.activity.resources.getString(R.string.title_reviews)
        )

        for (text in tabTitle) {
            val matcher = allOf(withText(text.toUpperCase()), isDescendantOfA(withId(R.id.tab_layout)))
            onView(matcher).check(matches(withText(text.toUpperCase())))
                    .perform(closeSoftKeyboard(), click())
                    .check(matches(isSelected()))
        }
    }

    @Test
    fun orientation_Test() {
        onView(allOf(withText(R.string.title_reviews), isDescendantOfA(withId(R.id.tab_layout))))
                .perform(closeSoftKeyboard())
                .perform(click())
        setOrientationLanscape(activityRule)
        onView(allOf(withText(R.string.title_reviews), isDescendantOfA(withId(R.id.tab_layout)))).check(matches(isSelected()))
    }
}
