package com.example.eastagile.themoviedatabase.authen

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.forgotpassword.ForgotPasswordActivity
import com.example.eastagile.themoviedatabase.authens.signin.SignInActivity
import com.example.eastagile.themoviedatabase.authens.signin.SignInPresenter
import com.example.eastagile.themoviedatabase.authens.signup.SignUpActivity
import com.example.eastagile.themoviedatabase.util.Utils
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.ERROR
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.FAKE_EMAIL
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.FAKE_PASSWORD
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify


class SignInUITest {

    @get:Rule
    val activityRule = ActivityTestRule(SignInActivity::class.java)

    @Before
    fun setUp() {
        activityRule.activity.mPresenter = mock(SignInPresenter::class.java)
    }

    @Test
    fun btnSignIn_InValid_Test() {
        onView(withId(R.id.btn_sign_in)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.sign_in)))
                .perform(closeSoftKeyboard(), click())

        verify(activityRule.activity.mPresenter).signIn(eq(""), eq(""),
                eq(Utils.getFavoriteDatabaseReference()))
    }

    @Test
    fun btnSignIn_Valid_Test() {
        onView(withId(R.id.edt_email)).perform(typeText(FAKE_EMAIL), closeSoftKeyboard())
        onView(withId(R.id.edt_password)).perform(typeText(FAKE_PASSWORD)).perform(closeSoftKeyboard())
        onView(withId(R.id.btn_sign_in)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.sign_in)))
                .perform(closeSoftKeyboard(), click())

        verify(activityRule.activity.mPresenter).signIn(eq(FAKE_EMAIL), eq(FAKE_PASSWORD),
                eq(Utils.getFavoriteDatabaseReference()))
    }

    @Test
    fun btnCancel_Test() {
        Intents.init()
        onView(withId(R.id.btn_cancel)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.cancel)))
                .perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
        Intents.release()
    }

    @Test
    fun tvForGotPassword_Click_Test() {
        Intents.init()
        onView(withId(R.id.tv_forgot_password)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.forgot_password)))
                .perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
        Intents.intended(hasComponent(ForgotPasswordActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun tvSignUp_Test() {
        Intents.init()
        onView(withText(R.string.sign_up)).check(matches(isDisplayed())).perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
        Intents.intended(hasComponent(SignUpActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun showError_Test() {
        activityRule.runOnUiThread { activityRule.activity.showError(ERROR) }

        onView(withId(R.id.tv_notify)).check(matches(withText(ERROR)))
    }

    @Test
    fun showSuccess_Test() {
        activityRule.runOnUiThread { activityRule.activity.showSuccess() }
        assertTrue(activityRule.activity.isFinishing)
    }
}
