package com.example.eastagile.themoviedatabase.detail

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.example.eastagile.themoviedatabase.AndroidTestUtils
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.base.TestActivity
import com.example.eastagile.themoviedatabase.util.MovieUtils.MOVIE_DETAIL_HEADER_TYPE
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailFragmentUITest {

    @get:Rule
    val activityRule = ActivityTestRule(TestActivity::class.java)

    val movie = AndroidTestUtils.makeSingleMovie(1)
    private val fakeHeaderType = "Popular"
    private val fragment: DetailFragment = DetailFragment.newInstance(movie)

    @Before
    fun setUp() {
        fragment.arguments?.putString(MOVIE_DETAIL_HEADER_TYPE, fakeHeaderType)
        activityRule.activity.supportFragmentManager.beginTransaction().replace(R.id.test_fragment, fragment).commit()
    }

    @Test
    fun headerBar_Test() {
        onView(withId(R.id.detail_header)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_detail_header_type)).check(matches(isDisplayed()))
                .check(matches(withText(fakeHeaderType)))
    }

    @Test
    fun updateMovie_Test() {
        val fakeTitle = "fake Title"
        val newMovie = AndroidTestUtils.makeSingleMovie(1)
        newMovie.title = fakeTitle
        activityRule.runOnUiThread { fragment.updateMovie(newMovie, fakeHeaderType) }
        onView(withId(R.id.tv_detail_header_movie_name)).check(matches(withText(fakeTitle.toUpperCase())))
    }
}
