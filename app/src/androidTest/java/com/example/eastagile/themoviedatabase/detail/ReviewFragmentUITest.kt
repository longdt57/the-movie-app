package com.example.eastagile.themoviedatabase.detail

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import com.example.eastagile.themoviedatabase.AndroidTestUtils.makeListReview
import com.example.eastagile.themoviedatabase.AndroidTestUtils.makeSingleMovie
import com.example.eastagile.themoviedatabase.AndroidTestUtils.withIndex
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.base.TestActivity
import com.example.eastagile.themoviedatabase.detail.fragment.reviews.ReviewFragment
import com.example.eastagile.themoviedatabase.detail.fragment.reviews.ReviewPresenter
import com.example.eastagile.themoviedatabase.model.Movie
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class ReviewFragmentUITest {

    @get:Rule
    val activityRule = ActivityTestRule(TestActivity::class.java)

    private lateinit var fragment: ReviewFragment
    private lateinit var movie: Movie

    @Before
    fun setUp() {
        movie = makeSingleMovie(1)
        fragment = ReviewFragment.newInstance(movie)
        fragment.mPresenter = mock(ReviewPresenter::class.java)

        activityRule.activity.supportFragmentManager.beginTransaction().replace(R.id.test_fragment, fragment).commit()
    }

    @Test
    fun presenter_Start_Test() {
        activityRule.runOnUiThread {}
        verify(fragment.mPresenter).start(eq(fragment))
    }

    @Test
    fun showReview_Test() {
        val numTrailer = 10
        val listTrailer = makeListReview(numTrailer)
        activityRule.runOnUiThread { fragment.showReviews(listTrailer) }
        assertTrue(fragment.mAdapter.list.size == numTrailer)
    }

    @Test
    fun list_Item_Click_Test() {
        val numItem = 10
        val itemPosClick = 9
        val list = makeListReview(numItem)
        activityRule.runOnUiThread { fragment.showReviews(list) }

        // click on item
        onView(withId(R.id.rcv_content))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemPosClick))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(itemPosClick, click()))
    }

    @Test
    fun show_Error_Test() {
        val fakeMessage = "error"

        activityRule.runOnUiThread { fragment.showError(fakeMessage) }
        onView(withText(fakeMessage)).check(matches(isDisplayed()))
    }

    @Test
    fun showSuccess() {
        activityRule.runOnUiThread {
            fragment.showSuccess()
        }

        onView(withText(R.string.success)).inRoot(withDecorView(not(
                `is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
    }

    @Test
    fun indicator_Show_Test() {
        activityRule.runOnUiThread {
            fragment.setLoadingIndicator(true)
        }

        onView(withIndex(withId(R.id.refresh_layout), 0)).check(matches(isDisplayingAtLeast(85)))
    }

    @Test
    fun updateMovie_Test() {
        val newMovie = makeSingleMovie(movie.id + 1)
        activityRule.runOnUiThread { fragment.updateMovie(newMovie) }
        assertTrue(newMovie.id == fragment.mMovie.id)
    }

    @Test
    fun fragmentArgument_empty_Test() {
        fragment = ReviewFragment.newInstance(movie)
        fragment.mPresenter = mock(ReviewPresenter::class.java)
        fragment.arguments = null
        activityRule.activity.supportFragmentManager.beginTransaction().replace(R.id.test_fragment, fragment).commit()

        assertNull(fragment.arguments)
    }

}
