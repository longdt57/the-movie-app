package com.example.eastagile.themoviedatabase.authen

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.forgotpassword.ForgotPasswordActivity
import com.example.eastagile.themoviedatabase.authens.forgotpassword.ForgotPasswordPresenter
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.ERROR
import com.example.eastagile.themoviedatabase.utils.AuthenUIUtils.Companion.FAKE_EMAIL
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class ForgotPasswordUITest {

    @get:Rule
    val activityRule = ActivityTestRule(ForgotPasswordActivity::class.java)

    @Before
    fun setUp() {
        activityRule.activity.mPresenter = mock(ForgotPasswordPresenter::class.java)
    }

    @Test
    fun btnSubmit_InValidValue_Test() {
        onView(withId(R.id.btn_submit)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.submit)))
                .perform(closeSoftKeyboard(), click())

        verify(activityRule.activity.mPresenter).sendPasswordResetEmail("")
    }

    @Test
    fun btnSubmit_ValidValue_Test() {
        onView(withId(R.id.edt_email)).perform(typeText(FAKE_EMAIL))
                .perform(closeSoftKeyboard())
        onView(withId(R.id.btn_submit))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.submit)))
                .perform(closeSoftKeyboard(), click())

        verify(activityRule.activity.mPresenter).sendPasswordResetEmail(FAKE_EMAIL)
    }

    @Test
    fun btnCancel_Test() {
        onView(withId(R.id.btn_cancel)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.cancel)))
                .perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
    }

    @Test
    fun tvNotify_Test() {
        onView(withId(R.id.tv_notify))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.restore_password_text)))
    }

    @Test
    fun showError_Test() {
        activityRule.runOnUiThread { activityRule.activity.showError(ERROR) }

        onView(withId(R.id.tv_notify)).check(matches(withText(ERROR)))
    }

    @Test
    fun showSuccess_Test() {
        activityRule.runOnUiThread { activityRule.activity.showSuccess() }

        assertTrue(activityRule.activity.isFinishing)
    }
}
