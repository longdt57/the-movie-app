package com.example.eastagile.themoviedatabase.authen

import android.content.Context
import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.signin.SignInActivity
import com.example.eastagile.themoviedatabase.authens.signup.SignUpActivity
import com.example.eastagile.themoviedatabase.authens.splash.SplashActivity
import com.example.eastagile.themoviedatabase.util.AuthenUtils
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SplashUITest {

    @get:Rule
    val activityRule = IntentsTestRule(SplashActivity::class.java, false, false)

    @Before
    fun setUp() {
        InstrumentationRegistry.getTargetContext().getSharedPreferences(AuthenUtils.AUTHEN_SHARE_PREFERENCE, Context.MODE_PRIVATE)
                .edit().putBoolean(AuthenUtils.SPLASH, false).apply()
        activityRule.launchActivity(Intent())
    }

    @Test
    fun btnSignIn_Test() {
        onView(withId(R.id.btn_sign_in)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.sign_in)))
                .perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
        intended(hasComponent(SignInActivity::class.java.name))
    }

    @Test
    fun btnSignUp_Test() {
        onView(withId(R.id.btn_sign_up)).check(matches(isDisplayed()))
                .check(matches(withText(R.string.sign_up)))
                .perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
        intended(hasComponent(SignUpActivity::class.java.name))
    }

    @Test
    fun tvSkip_Test() {
        onView(withId(R.id.tv_skip)).check(matches(isDisplayed()))
                .check(matches(withText(activityRule.activity.getString(R.string.or_skip))))
                .perform(closeSoftKeyboard(), click())

        assertTrue(activityRule.activity.isFinishing)
    }
}
