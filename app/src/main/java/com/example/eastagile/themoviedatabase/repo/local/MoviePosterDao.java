package com.example.eastagile.themoviedatabase.repo.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface MoviePosterDao {

    @Query("SELECT * FROM " + DBConstant.FAVOURITE_TABLE_NAME)
    Flowable<List<MoviePosterEntity>> getAllMovies();

    @Query("SELECT * FROM " + DBConstant.FAVOURITE_TABLE_NAME + " WHERE " + DBConstant.MOVIE_ID + " IN (:movieIds)")
    Flowable<List<MoviePosterEntity>> getMovies(int... movieIds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertMovies(List<MoviePosterEntity> movies);

    @Delete
    int deleteMovies(List<MoviePosterEntity> movies);

    @Query("DELETE FROM " + DBConstant.FAVOURITE_TABLE_NAME)
    void deleteAll();

}
