package com.example.eastagile.themoviedatabase.model;

public class MovieReview {
    public String author;
    public String content;
}
