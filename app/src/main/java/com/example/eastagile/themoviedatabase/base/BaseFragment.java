package com.example.eastagile.themoviedatabase.base;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eastagile.themoviedatabase.R;

import java.util.Objects;

public abstract class BaseFragment extends Fragment {

    public void setLoadingIndicator(boolean active) {
        SwipeRefreshLayout swipeRefreshLayout = Objects.requireNonNull(getView()).findViewById(R.id.refresh_layout);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(active);
        }
    }

    public void showError(String message) {
        setLoadingIndicator(false);
        TextView textView = Objects.requireNonNull(getView()).findViewById(R.id.tv_notify);
        if (textView != null) {
            int visibility = TextUtils.isEmpty(message) ? View.GONE : View.VISIBLE;

            textView.setText(TextUtils.isEmpty(message) ? "" : message);
            textView.setVisibility(visibility);
        }
    }

    public void showSuccess() {
        setLoadingIndicator(false);
        TextView textView = Objects.requireNonNull(getView()).findViewById(R.id.tv_notify);
        if (textView != null) {
            textView.setVisibility(View.GONE);
        }
        Toast.makeText(MovieApplication.getAppContext(),
                getString(R.string.success), Toast.LENGTH_SHORT).show();
    }

    protected SwipeRefreshLayout initSwipeRefreshLayout(View view) {
        // Set up progress indicator
        final SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.refresh_layout);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setColorSchemeColors(
                    ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorPrimary),
                    ContextCompat.getColor(getActivity(), R.color.colorAccent),
                    ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
            );
        }

        return swipeRefreshLayout;
    }
}
