package com.example.eastagile.themoviedatabase.base;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

public class MovieApplication extends MultiDexApplication {

    private static Context sAppContext;

    public static Context getAppContext() {
        return sAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sAppContext = getApplicationContext();
    }

}
