package com.example.eastagile.themoviedatabase.authens.forgotpassword

import com.example.eastagile.themoviedatabase.base.BasePresenter
import com.example.eastagile.themoviedatabase.base.BaseView

interface ForgotPasswordContract {
    interface View : BaseView<Presenter>

    interface Presenter : BasePresenter<View> {
        fun sendPasswordResetEmail(email: String?)
    }
}
