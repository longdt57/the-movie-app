package com.example.eastagile.themoviedatabase.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.authens.signin.SignInActivity;
import com.example.eastagile.themoviedatabase.authens.signup.SignUpActivity;
import com.example.eastagile.themoviedatabase.authens.splash.SplashActivity;
import com.google.firebase.auth.FirebaseAuth;

public abstract class BaseActivity extends AppCompatActivity {

    @VisibleForTesting
    public FirebaseAuth mAuth;

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    protected void initToolbar(@NonNull String... title) {
        // Init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);

        if (title.length > 0) {
            TextView tvTitle = toolbar.findViewById(R.id.tv_toolbar_title);
            tvTitle.setText(title[0]);
        }

        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.movie_menu_item, menu);

        boolean isSignIn = mAuth.getCurrentUser() != null;
        menu.findItem(R.id.item_sign_in).setVisible(!isSignIn);
        menu.findItem(R.id.item_sign_up).setVisible(!isSignIn);
        menu.findItem(R.id.item_sign_out).setVisible(isSignIn);

        menu.findItem(R.id.item_user).setTitle(isSignIn ?
                mAuth.getCurrentUser().getDisplayName() : getString(R.string.user_anonymous));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_sign_in:
                intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                break;

            case R.id.item_sign_up:
                intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                break;

            case R.id.item_sign_out:
                intent = new Intent(this, SplashActivity.class);
                startActivity(intent);
                mAuth.signOut();
                break;
        }

        return true;
    }
}
