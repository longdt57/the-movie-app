package com.example.eastagile.themoviedatabase.repo.webservice;

import com.example.eastagile.themoviedatabase.BuildConfig;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Implement Movie Network Repository
 */
public class MovieRemoteRepoImpl implements MovieRemoteRepo {

    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private Retrofit retrofit;

    private Retrofit getRetrofitInstance() {
        if (null == retrofit) {
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }

    @Override
    public Observable<MovieResult> getMoviesBy(MovieSortType sortOption) {
        return getRetrofitInstance().create(MovieDBService.class)
                .getMoviesBy(sortOption.value, BuildConfig.API_KEY);
    }

    @Override
    public Observable<MovieResult> getMoviesBy(MovieSortType sortOption, int page) {
        return getRetrofitInstance().create(MovieDBService.class)
                .getMoviesBy(sortOption.value, BuildConfig.API_KEY, page);
    }

    @Override
    public Observable<MovieTrailerResult> getMovieTrailers(String movieId) {
        return getRetrofitInstance().create(MovieDBService.class)
                .getMovieTrailers(movieId, BuildConfig.API_KEY);
    }

    @Override
    public Observable<MovieReviewResult> getMovieReviews(String movieId) {
        return getRetrofitInstance().create(MovieDBService.class)
                .getMovieReviews(movieId, BuildConfig.API_KEY);
    }

    public enum MovieSortType {
        POPULAR("popularity.desc"),
        RATE("vote_average.desc");

        public final String value;

        MovieSortType(String v) {
            value = v;
        }
    }
}
