package com.example.eastagile.themoviedatabase.authens.forgotpassword

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.base.BaseActivity
import com.google.common.annotations.VisibleForTesting
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.forgot_password.*

class ForgotPasswordActivity : BaseActivity(), View.OnClickListener, ForgotPasswordContract.View {

    @VisibleForTesting
    var mPresenter = ForgotPasswordPresenter(this, FirebaseAuth.getInstance())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgot_password)

        btn_cancel.setOnClickListener(this)
        btn_submit.setOnClickListener(this)

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_cancel -> finish()
            R.id.btn_submit -> {
                Toast.makeText(this, getString(R.string.verifying), Toast.LENGTH_SHORT).show()
                mPresenter.sendPasswordResetEmail(edt_email?.text?.toString())

            }
        }
    }

    override fun showSuccess() {
        Toast.makeText(this, getString(R.string.forgot_password_send_success_message), Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun showError(message: String?) {
        tv_notify?.run {
            visibility = View.VISIBLE
            text = message
            setTextColor(ContextCompat.getColor(context, R.color.notifyColor))
        }
    }

    override fun onStart() {
        super.onStart()
        mPresenter.start(this)
    }

    override fun onStop() {
        super.onStop()
        mPresenter.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearFindViewByIdCache()
    }

    override fun setLoadingIndicator(active: Boolean) {
    }

}
