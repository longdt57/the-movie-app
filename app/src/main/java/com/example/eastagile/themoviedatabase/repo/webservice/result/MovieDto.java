package com.example.eastagile.themoviedatabase.repo.webservice.result;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class MovieDto {

    @SerializedName("vote_count")
    public int voteCount;

    @SerializedName("id")
    public int id;

    @SerializedName("vote_average")
    public float voteAverage;

    @SerializedName("title")
    public String title;

    @SerializedName("popularity")
    public float popularity;

    @Nullable
    @SerializedName("poster_path")
    public String posterPath;

    @SerializedName("original_language")
    public String originalLanguage;

    @SerializedName("original_title")
    public String originalTitle;

    @SerializedName("genre_ids")
    public int[] genreIds;

    @Nullable
    @SerializedName("backdrop_path")
    public String backdropPath;

    @SerializedName("adult")
    public boolean adult;

    @SerializedName("overview")
    public String overview;

    @SerializedName("release_date")
    public String releaseDate;

    @SerializedName("video")
    public boolean video;

}
