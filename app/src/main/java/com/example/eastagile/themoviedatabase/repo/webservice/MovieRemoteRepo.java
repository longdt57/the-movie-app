package com.example.eastagile.themoviedatabase.repo.webservice;

import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;

import io.reactivex.Observable;

/**
 * Define Network movie repository function
 */

public interface MovieRemoteRepo {

    Observable<MovieResult> getMoviesBy(MovieRemoteRepoImpl.MovieSortType sortOption);

    Observable<MovieResult> getMoviesBy(MovieRemoteRepoImpl.MovieSortType sortOption, int page);

    Observable<MovieTrailerResult> getMovieTrailers(String movieId);

    Observable<MovieReviewResult> getMovieReviews(String movieId);

}
