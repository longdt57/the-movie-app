package com.example.eastagile.themoviedatabase.authens.signup

import android.content.Intent
import android.os.Bundle
import android.support.annotation.VisibleForTesting
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.signin.SignInActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.sign_up.*

class SignUpActivity : AppCompatActivity(), View.OnClickListener, SignUpContract.View {

    @VisibleForTesting
    var mPresenter = SignUpPresenter(this, FirebaseAuth.getInstance())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up)
        tv_sign_in.setOnClickListener(this)
        btn_cancel.setOnClickListener(this)
        btn_sign_up.setOnClickListener(this)

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_cancel -> finish()

            R.id.tv_sign_in -> {
                val intent = Intent(this, SignInActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.btn_sign_up -> {
                Toast.makeText(this, getString(R.string.verifying), Toast.LENGTH_SHORT).show()
                mPresenter.signUp(edt_email?.text?.toString(), edt_password?.text?.toString())
            }
            else -> {
            }

        }
    }

    override fun showError(message: String) {
        tv_notify?.visibility = View.VISIBLE
        tv_notify?.text = message
    }

    override fun showSuccess() {
        Toast.makeText(this, getString(R.string.success),
                Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onStart() {
        super.onStart()
        mPresenter.start(this)
    }

    override fun onStop() {
        super.onStop()
        mPresenter.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearFindViewByIdCache()
    }

    override fun setLoadingIndicator(active: Boolean) {
    }
}
