package com.example.eastagile.themoviedatabase.repo.webservice.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieReviewResult {

    @SerializedName("id")
    public int id;

    @SerializedName("page")
    public int page;

    @SerializedName("results")
    public List<MovieReviewDto> results;

    @SerializedName("total_pages")
    public int totalPages;

    @SerializedName("total_results")
    public int totalResults;

}
