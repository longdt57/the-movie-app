package com.example.eastagile.themoviedatabase.model;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * The Movie model which define a movie
 */

public class Movie implements Parcelable {

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
    public int id;
    public String title;
    public String posterPath;
    public float voteAverage;
    public String releaseDate;
    public String overview;
    // Favorite status
    public boolean isFavorite;

    public Movie() {
    }

    public Movie(Parcel in) {
        id = in.readInt();
        title = in.readString();
        posterPath = in.readString();
        voteAverage = in.readFloat();
        releaseDate = in.readString();
        overview = in.readString();
        isFavorite = in.readByte() != 0;
    }

    public Movie(int id, String title, String posterPath, float voteAverage, String releaseDate, String overview, boolean isFavorite) {
        this.id = id;
        this.title = title;
        this.posterPath = posterPath;
        this.voteAverage = voteAverage;
        this.releaseDate = releaseDate;
        this.overview = overview;
        this.isFavorite = isFavorite;
    }

    @Override
    public int describeContents() {
        return id;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(posterPath);
        parcel.writeFloat(voteAverage);
        parcel.writeString(releaseDate);
        parcel.writeString(overview);
        parcel.writeByte((byte) (isFavorite ? 1 : 0));
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Movie && id == ((Movie) obj).id;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = (31 * result * id) % Integer.MAX_VALUE;
        return result;
    }

}
