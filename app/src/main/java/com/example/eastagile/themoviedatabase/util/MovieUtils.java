package com.example.eastagile.themoviedatabase.util;

import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieDto;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;

import java.util.ArrayList;
import java.util.List;

public class MovieUtils {

    public static final String MOVIE_DETAIL_HEADER_TYPE = "detail_header_type";

    public static final String MOVIE_TAB = "movie_tab";

    public static final String MOVIE_CURRENT_SELECTED_ITEM = "current_item";

    // Prefix of poster image link
    private static final String POSTER_PREFIX = "https://image.tmdb.org/t/p/w500";

    private static String getFullMoviePosterPath(String posterPath) {
        return POSTER_PREFIX + (posterPath == null ? "" : posterPath);
    }


    /**
     * Convert local data to model
     *
     * @param listIn list local data
     * @return list of Movie data
     */
    public static List<Movie> convertEntityToMovie(List<MoviePosterEntity> listIn) {
        List<Movie> dataList = new ArrayList<>();

        for (MoviePosterEntity movieDto : listIn) {
            Movie movie = new Movie(movieDto.id,
                    movieDto.title,
                    movieDto.posterPath,
                    movieDto.voteAverage,
                    movieDto.releaseDate,
                    movieDto.overview,
                    true);
            dataList.add(movie);
        }

        return dataList;
    }

    /**
     * Convert Web API data to model, which can be use for UI view
     *
     * @param result Web API data
     * @return list of Movie data
     */
    public static List<Movie> convertDtoToMovie(MovieResult result) {
        List<Movie> dataList = new ArrayList<>();

        if (null != result && null != result.results) {

            for (MovieDto movieDto : result.results) {
                Movie movie = new Movie(movieDto.id,
                        movieDto.title,
                        getFullMoviePosterPath(movieDto.posterPath),
                        movieDto.voteAverage,
                        movieDto.releaseDate,
                        movieDto.overview,
                        false);
                dataList.add(movie);
            }
        }

        return dataList;
    }

    /**
     * Convert Movie data to Entity for saving
     *
     * @param items list movie
     * @return list Movie Entity
     */
    public static List<MoviePosterEntity> convertMovieToEntity(Movie... items) {
        List<MoviePosterEntity> list = new ArrayList<>();

        for (Movie item : items) {
            MoviePosterEntity entity = new MoviePosterEntity();
            entity.id = item.id;
            entity.title = item.title;
            entity.posterPath = item.posterPath;
            entity.voteAverage = item.voteAverage;
            entity.releaseDate = item.releaseDate;
            entity.overview = item.overview;
            list.add(entity);
        }
        return list;
    }

}
