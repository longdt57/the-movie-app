package com.example.eastagile.themoviedatabase.home.fragment;

import android.support.annotation.NonNull;

import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.MovieRepo;
import com.example.eastagile.themoviedatabase.util.MovieUtils;
import com.example.eastagile.themoviedatabase.util.Utils;
import com.google.common.primitives.Ints;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

import static com.example.eastagile.themoviedatabase.home.fragment.MovieTabType.MY_FAVORITE;
import static com.example.eastagile.themoviedatabase.home.fragment.MovieTabType.POPULAR;
import static com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepoImpl.MovieSortType;
import static io.reactivex.Observable.just;


public class MovieTabPresenter implements MovieTabContract.Presenter {

    private final MovieRepo mMovieRepo;
    private final Scheduler mMainScheduler;
    private final Scheduler mSubScheduler;
    private Disposable mDisposable;
    private MovieTabContract.View mView;
    private boolean mFirstLoad = true;

    public MovieTabPresenter(MovieRepo repo, Scheduler mainScheduler, Scheduler subScheduler) {
        this.mMovieRepo = repo;
        mMainScheduler = mainScheduler;
        mSubScheduler = subScheduler;
    }

    @Override
    public void loadMovies(MovieTabType tab, boolean forceUpdate, boolean isLogin,
                           DatabaseReference reference) {
        mView.setLoadingIndicator(forceUpdate || mFirstLoad);
        if (forceUpdate || mFirstLoad) {
            switch (tab) {
                case POPULAR:
                    getRemoteMoviesByFilter(MovieSortType.POPULAR);
                    break;
                case MOST_RATE:
                    getRemoteMoviesByFilter(MovieSortType.RATE);
                    break;
                default:
                    getMyFavoriteMovies(isLogin, reference);
                    break;
            }
        }

        mFirstLoad = false;
    }

    @Override
    public void loadMoreMovies(MovieTabType tab, int currentPage) {
        if (tab != MY_FAVORITE) {
            mView.setLoadingIndicator(true);
            int loadedPage = currentPage + 1;
            MovieSortType sortType = tab == POPULAR ? MovieSortType.POPULAR : MovieSortType.RATE;
            mDisposable = mMovieRepo.getMoviesBy(sortType, loadedPage)
                    .subscribeOn(mSubScheduler)
                    .observeOn(mMainScheduler)
                    .flatMap(movieResult -> just(MovieUtils.convertDtoToMovie(movieResult)))
                    .subscribe(movies -> mView.showMoreMovies(movies, loadedPage),
                            throwable -> mView.showError(throwable.getMessage()));
        }
    }

    private void getRemoteMoviesByFilter(MovieSortType sortType) {
        mDisposable = mMovieRepo.getMoviesBy(sortType)
                .subscribeOn(mSubScheduler)
                .observeOn(mMainScheduler)
                .flatMap(movieResult -> just(MovieUtils.convertDtoToMovie(movieResult)))
                .subscribe(
                        movies ->
                                mView.showMovies(movies)
                        ,
                        throwable -> mView.showError(throwable.getMessage())
                );

    }

    private void getMyFavoriteMovies(boolean isLogin, DatabaseReference reference) {
        if (isLogin) {
            getFavoriteMovieLogin(reference, true);
        } else {
            mDisposable = mMovieRepo.getMoviesFavourite()
                    .subscribeOn(mSubScheduler)
                    .observeOn(mMainScheduler)
                    .flatMap(moviePosterEntities -> Flowable.just(MovieUtils.convertEntityToMovie(moviePosterEntities)))
                    .subscribe(movies -> mView.showMovies(movies),
                            throwable -> mView.showError(throwable.getMessage()
                            ));
        }
    }

    @Override
    public void getMyFavoriteMoviesWithId(List<Movie> movieIds, boolean isLogIn, DatabaseReference reference) {
        if (isLogIn) {
            getFavoriteMovieLogin(reference, false);
        } else {
            List<Integer> listId = new ArrayList<>();
            for (Movie item : movieIds) {
                listId.add(item.id);
            }

            int[] array = Ints.toArray(listId);
            mDisposable = mMovieRepo.getMoviesFavoriteById(array)
                    .subscribeOn(mSubScheduler)
                    .observeOn(mMainScheduler)
                    .flatMap(moviePosterEntities -> Flowable.just(MovieUtils.convertEntityToMovie(moviePosterEntities)))
                    .subscribe(movies -> mView.updateMovieFavoriteStatus(movies),
                            throwable -> mView.showError(throwable.getMessage()
                            ));
        }
    }

    @Override
    public void onFavoriteClick(Movie items, boolean isLogin, DatabaseReference reference) {
        Utils.onFavoriteClick(items, isLogin, reference, mMovieRepo, mDisposable, mMainScheduler, mSubScheduler, mView);

    }

    @Override
    public void start(MovieTabContract.View view) {
        mView = view;
    }

    @Override
    public void stop() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
        mView = null;
    }

    private void getFavoriteMovieLogin(DatabaseReference reference, boolean isNewList) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Movie> list = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    list.add(snapshot.getValue(Movie.class));
                }
                if (mView != null) {
                    if (isNewList) {
                        mView.showMovies(list);
                    } else {
                        mView.updateMovieFavoriteStatus(list);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                if (mView != null) {
                    mView.showError(databaseError.getMessage());
                }
            }
        };

        reference.addValueEventListener(postListener);
    }
}
