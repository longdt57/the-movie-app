package com.example.eastagile.themoviedatabase.adapter;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.model.MovieTrailer;
import com.example.eastagile.themoviedatabase.util.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.eastagile.themoviedatabase.util.Utils.convertReleaseDate;

/**
 * This list include header and normal trailer item
 * Header include movie detail: title, rate, release date, content description
 */
public class MovieTrailerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private final List<MovieTrailer> dataList;
    private final OnItemClickListener listener;
    private Movie movie = new Movie();

    public MovieTrailerAdapter(OnItemClickListener listener) {
        dataList = new ArrayList<>();
        this.listener = listener;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @NonNull
    @Override
    public VHItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.movie_trailer_header, viewGroup, false);
            return new VHHeader(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.movie_trailer_item, viewGroup, false);
            return new VHItem(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof VHHeader) {
            VHHeader vhHeader = (VHHeader) viewHolder;
            if (movie != null) {
                vhHeader.loadImage(movie.posterPath);
                vhHeader.tvTitle.setText(movie.title.toUpperCase());
                vhHeader.rbRating.setRating(movie.voteAverage);
                vhHeader.tvReleaseDate.setText(convertReleaseDate(movie.releaseDate));
                vhHeader.tvContent.setText(movie.overview);
                vhHeader.imvFavorite.setImageDrawable(ContextCompat.getDrawable(vhHeader.itemView.getContext(),
                        movie.isFavorite ? R.drawable.btn_favourite_on : R.drawable.btn_favourite_off));
                vhHeader.imvFavorite.setOnClickListener(view -> {
                    movie.isFavorite = !movie.isFavorite;
                    notifyItemChanged(viewHolder.getAdapterPosition());
                    listener.onFavoriteClick(movie);
                });
            }

        } else {
            VHItem vhItem = (VHItem) viewHolder;
            final MovieTrailer item = dataList.get(position - 1);
            vhItem.tvTitle.setText(item.title);
            vhItem.itemView.setOnClickListener(view -> listener.onItemClick(item));
            vhItem.loadImage(item.imagePath);
        }

    }

    public List<MovieTrailer> getList() {
        return dataList;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return dataList.size() + 1;
    }

    public interface OnItemClickListener {
        void onItemClick(MovieTrailer item);

        void onFavoriteClick(Movie item);
    }

    class VHItem extends RecyclerView.ViewHolder {

        ImageView imvThumbnail;
        TextView tvTitle;

        VHItem(@NonNull View view) {
            super(view);
            imvThumbnail = itemView.findViewById(R.id.imv_trailer);
            tvTitle = itemView.findViewById(R.id.tv_trailer_title);
        }

        void loadImage(String url) {
            if (!TextUtils.isEmpty(url)) {
                itemView.findViewById(R.id.imv_loading).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.imv_play).setVisibility(View.GONE);

                Picasso.get()
                        .load(url)
                        .error(R.drawable.ic_error)
                        .into(imvThumbnail, new Callback() {
                            @Override
                            public void onSuccess() {
                                itemView.findViewById(R.id.imv_loading).setVisibility(View.GONE);
                                itemView.findViewById(R.id.imv_play).setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                itemView.findViewById(R.id.imv_loading).setVisibility(View.GONE);
                                itemView.findViewById(R.id.imv_play).setVisibility(View.GONE);
                            }
                        });
            }
        }
    }

    class VHHeader extends VHItem {

        ImageView imvPoster;
        TextView tvTitle;
        RatingBar rbRating;
        TextView tvReleaseDate;
        TextView tvContent;
        ImageView imvFavorite;
        ImageView imvLoading;

        VHHeader(View view) {
            super(view);
            imvPoster = itemView.findViewById(R.id.imv_poster);
            tvTitle = itemView.findViewById(R.id.tv_title);
            rbRating = itemView.findViewById(R.id.rb_rating);
            tvReleaseDate = itemView.findViewById(R.id.tv_release_date);
            tvContent = itemView.findViewById(R.id.tv_content);

            ViewTreeObserver observer = tvContent.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int maxLines = tvContent.getHeight()
                            / tvContent.getLineHeight();
                    tvContent.setMaxLines(maxLines);
                    tvContent.getViewTreeObserver().removeOnGlobalLayoutListener(
                            this);
                }
            });
            imvFavorite = itemView.findViewById(R.id.imv_trailer_favorite);
            imvLoading = itemView.findViewById(R.id.imv_loading);

        }

        void loadImage(String url) {
            Utils.loadImage(url, imvPoster, imvLoading, itemView.getContext());
        }
    }
}
