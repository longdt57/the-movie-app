package com.example.eastagile.themoviedatabase.authens.signin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.forgotpassword.ForgotPasswordActivity
import com.example.eastagile.themoviedatabase.authens.signup.SignUpActivity
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl
import com.example.eastagile.themoviedatabase.util.Utils
import com.google.common.annotations.VisibleForTesting
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.sign_in.*

class SignInActivity : AppCompatActivity(), View.OnClickListener, SignInContract.View {

    @VisibleForTesting
    var mPresenter = SignInPresenter(this, FirebaseAuth.getInstance(), MovieRepoImpl.newInstance())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_in)

        btn_sign_in.setOnClickListener(this)
        btn_cancel.setOnClickListener(this)
        tv_sign_up.setOnClickListener(this)
        tv_forgot_password.setOnClickListener(this)

    }

    override fun onClick(view: View) {
        var intent: Intent? = null
        when (view.id) {
            R.id.btn_cancel -> finish()

            R.id.btn_sign_in -> {
                Toast.makeText(this, getString(R.string.verifying), Toast.LENGTH_SHORT).show()
                mPresenter.signIn(edt_email?.text?.toString(), edt_password?.text?.toString(),
                        Utils.getFavoriteDatabaseReference())
            }

            R.id.tv_sign_up -> intent = Intent(this, SignUpActivity::class.java)

            R.id.tv_forgot_password -> intent = Intent(this, ForgotPasswordActivity::class.java)
            else -> {
            }
        }
        if (intent != null) {
            startActivity(intent)
            finish()
        }
    }


    override fun showSuccess() {
        Toast.makeText(this, getString(R.string.success),
                Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun showError(message: String) {
        tv_notify?.text = message
        tv_notify?.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        mPresenter.start(this)
    }

    override fun onStop() {
        super.onStop()
        mPresenter.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearFindViewByIdCache()
    }

    override fun setLoadingIndicator(active: Boolean) {
    }

}
