package com.example.eastagile.themoviedatabase.repo.webservice;

import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Define Network connection function
 */

public interface MovieDBService {

    @GET("movie/{id}/reviews")
    Observable<MovieReviewResult> getMovieReviews(@Path("id") String id, @Query("api_key") String api_key);

    @GET("movie/{id}/videos")
    Observable<MovieTrailerResult> getMovieTrailers(@Path("id") String id, @Query("api_key") String api_key);

    @GET("discover/movie")
    Observable<MovieResult> getMoviesBy(@Query("sort_by") String sortOption, @Query("api_key") String api_key);

    @GET("discover/movie")
    Observable<MovieResult> getMoviesBy(@Query("sort_by") String sortOption,
                                        @Query("api_key") String api_key,
                                        @Query("page") int page);

}
