package com.example.eastagile.themoviedatabase.authens.signin

import com.example.eastagile.themoviedatabase.model.Movie
import com.example.eastagile.themoviedatabase.repo.MovieRepo
import com.example.eastagile.themoviedatabase.util.AuthenUtils
import com.example.eastagile.themoviedatabase.util.MovieUtils
import com.example.eastagile.themoviedatabase.util.Utils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

open class SignInPresenter(private var mView: SignInContract.View?,
                           private val mAuth: FirebaseAuth,
                           private val movieRepo: MovieRepo) : SignInContract.Presenter {
    private var disposable: Disposable? = null

    override fun stop() {
        mView = null
        disposable?.dispose()
    }

    override fun signIn(email: String?, password: String?, reference: DatabaseReference?) {
        if (Utils.isEmpty(email) || Utils.isEmpty(password)) {
            mView?.showError(AuthenUtils.ERROR_EMPTY)
        } else {
            mAuth.signInWithEmailAndPassword(email!!, password!!)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mView?.showSuccess()
                            syncData(reference)
                        } else {
                            task.exception?.message?.let { mView?.showError(it) }
                        }
                    }
        }
    }

    override fun start(view: SignInContract.View) {
        mView = view
    }

    /**
     * Sync data with local
     */
    private fun syncData(reference: DatabaseReference?) {
        disposable = movieRepo.moviesFavourite
                .flatMap { moviePosterEntities -> Flowable.just(MovieUtils.convertEntityToMovie(moviePosterEntities)) }
                .subscribe(
                        { movies ->
                            addMovieToFirebase(movies, reference)
                            movieRepo.deleteFavoriteAll().subscribeOn(Schedulers.io()).subscribe()
                        },
                        { throwable ->
                            mView?.showError(throwable?.message)
                        }
                )
    }

    private fun addMovieToFirebase(movies: MutableList<Movie>, reference: DatabaseReference?) {
        for (movie in movies) {
            reference?.child(movie.id.toString())?.setValue(movie)
        }
    }

}
