package com.example.eastagile.themoviedatabase.repo.webservice.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Define result from get movie API
 */

public class MovieResult {

    @SerializedName("page")
    public int page;

    @SerializedName("results")
    public List<MovieDto> results;

    @SerializedName("total_results")
    public int totalResults;

    @SerializedName("total_pages")
    public int totalPages;

}
