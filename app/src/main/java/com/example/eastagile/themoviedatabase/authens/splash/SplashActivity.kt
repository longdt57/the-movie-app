package com.example.eastagile.themoviedatabase.authens.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

import com.example.eastagile.themoviedatabase.R
import com.example.eastagile.themoviedatabase.authens.signin.SignInActivity
import com.example.eastagile.themoviedatabase.authens.signup.SignUpActivity
import com.example.eastagile.themoviedatabase.home.MovieActivity
import com.example.eastagile.themoviedatabase.util.AuthenUtils.AUTHEN_SHARE_PREFERENCE
import com.example.eastagile.themoviedatabase.util.AuthenUtils.SPLASH
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Check to show splash screen
        val preferences = getSharedPreferences(AUTHEN_SHARE_PREFERENCE, Context.MODE_PRIVATE)
        val splashIsDisplayed = preferences.getBoolean(SPLASH, false)
        if (splashIsDisplayed) {
            val intent = Intent(this, MovieActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            setContentView(R.layout.activity_splash)

            btn_sign_in.setOnClickListener(this)
            btn_sign_up.setOnClickListener(this)
            tv_skip.setOnClickListener(this)
        }

        preferences.edit().putBoolean(SPLASH, true).apply()

    }

    override fun onClick(view: View) {
        val intent = when (view.id) {
            R.id.btn_sign_in -> Intent(this, SignInActivity::class.java)
            R.id.btn_sign_up -> Intent(this, SignUpActivity::class.java)
            else -> Intent(this, MovieActivity::class.java)
        }
        startActivity(intent)
        finish()
    }
}
