package com.example.eastagile.themoviedatabase.home.fragment;

import com.example.eastagile.themoviedatabase.base.BasePresenter;
import com.example.eastagile.themoviedatabase.base.BaseView;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

public interface MovieTabContract {
    interface View extends BaseView<Presenter> {

        void showMovies(List<Movie> list);

        void showMoreMovies(List<Movie> list, int loadedPage);

        void updateMovieFavoriteStatus(List<Movie> favoriteList);

    }

    interface Presenter extends BasePresenter<View> {

        void loadMovies(MovieTabType tab, boolean forceUpdate, boolean isLogin,
                        DatabaseReference reference);

        void loadMoreMovies(MovieTabType tab, int currentPage);

        // Get movie from local db with movie ids
        void getMyFavoriteMoviesWithId(List<Movie> list, boolean isLogIn, DatabaseReference reference);

        void onFavoriteClick(Movie items, boolean isLogin, DatabaseReference reference);

    }
}
