package com.example.eastagile.themoviedatabase.detail.fragment.trailer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.adapter.MovieTrailerAdapter;
import com.example.eastagile.themoviedatabase.base.BaseFragment;
import com.example.eastagile.themoviedatabase.detail.DetailFragment;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.model.MovieTrailer;
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;
import com.example.eastagile.themoviedatabase.util.Utils;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.example.eastagile.themoviedatabase.util.MovieDetailUtils.MOVIE_OBJECT;


public class TrailerFragment extends BaseFragment
        implements MovieTrailerAdapter.OnItemClickListener,
        TrailerContract.View, DetailFragment.ChildFragmentInterface {

    @VisibleForTesting
    public final MovieTrailerAdapter mAdapter = new MovieTrailerAdapter(this);
    @VisibleForTesting
    public TrailerContract.Presenter mPresenter = new TrailerPresenter(MovieRepoImpl.newInstance(),
            this, AndroidSchedulers.mainThread(), Schedulers.io());
    private Movie mMovie = new Movie();


    public static TrailerFragment newInstance(Movie movie) {
        TrailerFragment fragment = new TrailerFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(MOVIE_OBJECT, movie);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get data
        if (getArguments() != null) {
            mMovie = getArguments().getParcelable(MovieDetailUtils.MOVIE_OBJECT);
        }

        mAdapter.setMovie(mMovie);

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start(this);
        mPresenter.loadTrailers(String.valueOf(mMovie.id), false);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trailer, container, false);

        // Init detail header with width is twice
        int headerSpanCount = 2;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), headerSpanCount);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mAdapter.getItemViewType(position)) {
                    case MovieTrailerAdapter.TYPE_HEADER:
                        return headerSpanCount;
                    default:
                        return 1;
                }
            }
        });

        RecyclerView mRecyclerView = view.findViewById(R.id.rcv_content);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        // Set up progress indicator
        final SwipeRefreshLayout mSwipeRefreshLayout = initSwipeRefreshLayout(view);
        // Set up pull down to refresh
        mSwipeRefreshLayout.setOnRefreshListener(
                () -> mPresenter.loadTrailers(String.valueOf(mMovie.id), true));

        return view;
    }

    @Override
    public void onItemClick(MovieTrailer item) {
        if (URLUtil.isValidUrl(item.videoPath)) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.videoPath));
            Intent chooser = Intent.createChooser(intent, "");
            // Verify the original intent will resolve to at least one activity
            if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
                startActivity(chooser);
            }
        }
    }

    @Override
    public void onFavoriteClick(Movie item) {
        mPresenter.onFavoriteClick(item, FirebaseAuth.getInstance().getCurrentUser() != null,
                Utils.getFavoriteDatabaseReference());
    }

    @Override
    public void showTrailers(List<MovieTrailer> list) {
        mAdapter.getList().clear();
        mAdapter.getList().addAll(list);
        mAdapter.notifyDataSetChanged();

        setLoadingIndicator(false);

        String message = list.isEmpty() ? getString(R.string.refresh) : "";
        showError(message);
    }

    @Override
    public void updateMovie(Movie movie) {
        this.mMovie = movie;
        mAdapter.setMovie(movie);
        mAdapter.notifyItemChanged(0);

        mPresenter.loadTrailers(String.valueOf(movie.id), true);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.stop();
    }

}
