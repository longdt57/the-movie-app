package com.example.eastagile.themoviedatabase.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.crashlytics.android.Crashlytics;
import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.base.BaseActivity;
import com.example.eastagile.themoviedatabase.base.BaseViewPagerAdapter;
import com.example.eastagile.themoviedatabase.base.ZoomOutPageTransformer;
import com.example.eastagile.themoviedatabase.detail.DetailActivity;
import com.example.eastagile.themoviedatabase.detail.DetailFragment;
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabFragment;
import com.example.eastagile.themoviedatabase.home.fragment.MovieTabType;
import com.example.eastagile.themoviedatabase.model.Movie;

import java.util.Objects;

import io.fabric.sdk.android.Fabric;

import static com.example.eastagile.themoviedatabase.util.MovieDetailUtils.MOVIE_OBJECT;
import static com.example.eastagile.themoviedatabase.util.MovieUtils.MOVIE_DETAIL_HEADER_TYPE;

/**
 * This Activity handle Main Screen UI.
 * Show Movie poster in type of Popular, Rate, Favorite
 */

public class MovieActivity extends BaseActivity implements MovieTabFragment.PosterClickListener {

    private final int[] mTabTitle = {
            R.string.title_popular,
            R.string.title_most_rated,
            R.string.title_my_fav
    };
    private final MovieTabType[] mTabTypes = {
            MovieTabType.POPULAR,
            MovieTabType.MOST_RATE,
            MovieTabType.MY_FAVORITE
    };
    @VisibleForTesting
    public BaseViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_movie);

        initToolbar();

        // Tab
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        for (int aMTabTitle : mTabTitle) {
            tabLayout.addTab(tabLayout.newTab()
                    .setText(getString(aMTabTitle)));
        }

        viewPager = findViewById(R.id.vp_container);
        viewPagerAdapter = new BaseViewPagerAdapter(getSupportFragmentManager());

        for (int i = 0; i < mTabTitle.length; i++) {
            viewPagerAdapter.addFragment(
                    MovieTabFragment.newInstance(mTabTypes[i]), getString(mTabTitle[i]));
        }

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        tabLayout.setupWithViewPager(viewPager);

        Fabric.with(this, new Crashlytics());
        Crashlytics.log("Start the Movie App");
    }

    @Override
    public void onPosterClick(Movie item) {
        if (findViewById(R.id.fl_movie_right) != null) {
            DetailFragment fragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.fl_movie_right);
            String movieTabTitle = Objects.requireNonNull(viewPagerAdapter.getPageTitle(viewPager.getCurrentItem())).toString();

            if (fragment == null) {
                fragment = DetailFragment.newInstance(item);
                Objects.requireNonNull(fragment.getArguments()).putString(MOVIE_DETAIL_HEADER_TYPE, movieTabTitle);

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fl_movie_right, fragment)
                        .addToBackStack(null)
                        .commit();
            } else {
                ((RightFragmentInterface) fragment).updateMovie(item, movieTabTitle);
            }

        } else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(MOVIE_OBJECT, item);
            startActivity(intent);
        }

    }

    public interface RightFragmentInterface {
        void updateMovie(Movie movie, String movieTabTitle);
    }
}
