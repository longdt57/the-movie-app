package com.example.eastagile.themoviedatabase.authens.signup

import android.support.annotation.VisibleForTesting
import com.example.eastagile.themoviedatabase.base.BasePresenter
import com.example.eastagile.themoviedatabase.base.BaseView

@VisibleForTesting
interface SignUpContract {
    interface View : BaseView<Presenter>

    interface Presenter : BasePresenter<View> {
        fun signUp(email: String?, password: String?)
    }
}
