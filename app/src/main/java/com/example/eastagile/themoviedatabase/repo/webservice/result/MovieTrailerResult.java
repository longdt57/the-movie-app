package com.example.eastagile.themoviedatabase.repo.webservice.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieTrailerResult {

    @SerializedName("id")
    public int id;

    @SerializedName("results")
    public List<MovieTrailerDto> results;

}
