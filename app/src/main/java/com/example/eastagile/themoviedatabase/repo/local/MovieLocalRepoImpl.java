package com.example.eastagile.themoviedatabase.repo.local;

import android.arch.persistence.room.Room;
import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;


public class MovieLocalRepoImpl implements MovieLocalRepo {

    private final MovieDatabase movieDatabase;

    public MovieLocalRepoImpl(@NotNull Context context) {
        movieDatabase = Room.databaseBuilder(context.getApplicationContext(),
                MovieDatabase.class, DBConstant.DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    /**
     * Get all user favorite movie
     *
     * @return favorite movie list
     */
    @Override
    public Flowable<List<MoviePosterEntity>> getAllFavoriteMovies() {
        return movieDatabase.moviePosterDao().getAllMovies();
    }

    /**
     * Get favorite movie by id
     *
     * @param movieIds list of movie ids
     * @return list of movie entity
     */
    @Override
    public Flowable<List<MoviePosterEntity>> getFavoriteMoviesByIds(int... movieIds) {
        return movieDatabase.moviePosterDao().getMovies(movieIds);
    }

    @Override
    public Completable addFavoriteMovies(List<MoviePosterEntity> items) {
        return Completable.fromAction(() -> movieDatabase.moviePosterDao().insertMovies(items));

    }

    @Override
    public Completable removeFavoriteMovies(List<MoviePosterEntity> items) {
        return Completable.fromAction(() -> movieDatabase.moviePosterDao().deleteMovies(items));
    }

    @Override
    public Completable deleteAll() {
        return Completable.fromAction(() -> movieDatabase.moviePosterDao().deleteAll());
    }
}
