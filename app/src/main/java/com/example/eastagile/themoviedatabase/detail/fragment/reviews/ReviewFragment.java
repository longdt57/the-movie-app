package com.example.eastagile.themoviedatabase.detail.fragment.reviews;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.adapter.MovieReviewAdapter;
import com.example.eastagile.themoviedatabase.base.BaseFragment;
import com.example.eastagile.themoviedatabase.detail.DetailFragment;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.model.MovieReview;
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.example.eastagile.themoviedatabase.util.MovieDetailUtils.MOVIE_OBJECT;

public class ReviewFragment extends BaseFragment implements ReviewContract.View,
        DetailFragment.ChildFragmentInterface {

    @VisibleForTesting
    public MovieReviewAdapter mAdapter = new MovieReviewAdapter();
    RecyclerView mRecyclerView;

    @VisibleForTesting
    public ReviewContract.Presenter mPresenter = new ReviewPresenter(MovieRepoImpl.newInstance(),
            this, AndroidSchedulers.mainThread(), Schedulers.io());

    @VisibleForTesting
    public Movie mMovie = new Movie();


    public static ReviewFragment newInstance(Movie mMovie) {

        ReviewFragment fragment = new ReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(MOVIE_OBJECT, mMovie);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get data
        if (getArguments() != null) {
            mMovie = getArguments().getParcelable(MovieDetailUtils.MOVIE_OBJECT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start(this);
        mPresenter.loadReviews(String.valueOf(mMovie.id), false);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.stop();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review, container, false);

        mRecyclerView = view.findViewById(R.id.rcv_content);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Set up progress indicator
        final SwipeRefreshLayout mSwipeRefreshLayout = initSwipeRefreshLayout(view);
        // Set up pull down to refresh
        mSwipeRefreshLayout.setOnRefreshListener(
                () -> mPresenter.loadReviews(String.valueOf(mMovie.id), true));

        return view;
    }

    @Override
    public void showReviews(List<MovieReview> list) {
        mAdapter = new MovieReviewAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.getList().addAll(list);
        mAdapter.notifyDataSetChanged();

        setLoadingIndicator(false);

        String message = list.isEmpty() ? getString(R.string.refresh) : "";
        showError(message);
    }

    @Override
    public void updateMovie(Movie movie) {
        this.mMovie = movie;
        mAdapter = new MovieReviewAdapter();
        mPresenter.loadReviews(String.valueOf(movie.id), true);
    }

}
