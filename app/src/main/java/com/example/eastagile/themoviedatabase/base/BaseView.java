package com.example.eastagile.themoviedatabase.base;

public interface BaseView<T> {

    void setLoadingIndicator(boolean active);

    void showError(String message);

    void showSuccess();

}
