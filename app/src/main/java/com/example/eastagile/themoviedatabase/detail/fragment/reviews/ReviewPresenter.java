package com.example.eastagile.themoviedatabase.detail.fragment.reviews;

import android.support.annotation.VisibleForTesting;

import com.example.eastagile.themoviedatabase.repo.MovieRepo;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;

public class ReviewPresenter implements ReviewContract.Presenter {

    @VisibleForTesting
    public final CompositeDisposable mComposite = new CompositeDisposable();
    private final MovieRepo mMovieRepo;
    private final Scheduler mMainScheduler;
    private final Scheduler mSubScheduler;
    @VisibleForTesting
    public boolean mFirstLoad = true;
    private ReviewContract.View mView;

    public ReviewPresenter(MovieRepo movieRepo, ReviewContract.View reviewViewInterface,
                           Scheduler main, Scheduler sub) {
        mView = reviewViewInterface;
        mMovieRepo = movieRepo;

        mMainScheduler = main;
        mSubScheduler = sub;
    }

    @Override
    public void loadReviews(String movieId, boolean forceLoad) {
        if (mFirstLoad || forceLoad) {
            mFirstLoad = false;

            mComposite.add(mMovieRepo.getMovieReviews(movieId)
                    .subscribeOn(mSubScheduler)
                    .observeOn(mMainScheduler)
                    .subscribe(movieReviewResult -> mView.showReviews(MovieDetailUtils.convertReviews(movieReviewResult)),
                            throwable -> mView.showError(throwable.getMessage())));
        }
    }

    @Override
    public void start(ReviewContract.View view) {
        mView.setLoadingIndicator(false);
    }

    @Override
    public void stop() {
        mComposite.dispose();
        mView = null;
    }
}
