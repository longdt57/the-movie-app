package com.example.eastagile.themoviedatabase.util;

import android.support.annotation.NonNull;

import com.example.eastagile.themoviedatabase.model.MovieReview;
import com.example.eastagile.themoviedatabase.model.MovieTrailer;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewDto;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerDto;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MovieDetailUtils {

    // for save instance
    public static final String CURRENT_TAB = "current_tab";

    public static final String MOVIE_OBJECT = "movie_object";

    private static final String TRAILER_VIDEO_PREFIX = "https://www.youtube.com/watch?v=";
    private static final String TRAILER_IMAGE = "https://img.youtube.com/vi/%s/%d.jpg";
    private static final int TRAILER_IMAGE_POS = 2;

    public static List<MovieTrailer> convertTrailers(MovieTrailerResult results) {
        List<MovieTrailer> list = new ArrayList<>();

        if (results != null && results.results != null) {
            for (MovieTrailerDto item : results.results) {
                MovieTrailer movieTrailer = new MovieTrailer();
                movieTrailer.title = item.name;
                movieTrailer.videoPath = getTrailerVideoPath(item.key);
                movieTrailer.imagePath = getTrailerThumbnailPath(item.key);
                list.add(movieTrailer);
            }
        }

        return list;
    }

    private static String getTrailerVideoPath(@NonNull String path) {
        return TRAILER_VIDEO_PREFIX + path;
    }

    /**
     * That is 4 thumbnail images. Take random 2
     *
     * @param path trailer key
     * @return trailer thumbnail
     */
    private static String getTrailerThumbnailPath(@NonNull String path) {
        return String.format(Locale.US, TRAILER_IMAGE, path, TRAILER_IMAGE_POS);
    }

    public static List<MovieReview> convertReviews(MovieReviewResult results) {

        List<MovieReview> list = new ArrayList<>();

        if (results != null && results.results != null) {
            for (MovieReviewDto item : results.results) {
                MovieReview review = new MovieReview();
                review.author = item.author;
                review.content = item.content;

                list.add(review);
            }
        }

        return list;
    }


}
