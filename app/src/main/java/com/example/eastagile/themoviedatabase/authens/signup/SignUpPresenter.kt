package com.example.eastagile.themoviedatabase.authens.signup

import com.example.eastagile.themoviedatabase.util.AuthenUtils
import com.example.eastagile.themoviedatabase.util.Utils
import com.google.firebase.auth.FirebaseAuth

open class SignUpPresenter(private var mView: SignUpContract.View?,
                           private var mAuth: FirebaseAuth) : SignUpContract.Presenter {

    override fun signUp(email: String?, password: String?) {
        if (Utils.isEmpty(email) || Utils.isEmpty(password)) {
            mView?.showError(AuthenUtils.ERROR_EMPTY)
        } else {
            mAuth.createUserWithEmailAndPassword(email!!, password!!)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mView?.showSuccess()
                        } else {
                            task.exception?.message?.let { mView?.showError(it) }
                        }
                    }
        }
    }

    override fun start(view: SignUpContract.View) {
        mView = view
    }

    override fun stop() {
        mView = null
    }
}
