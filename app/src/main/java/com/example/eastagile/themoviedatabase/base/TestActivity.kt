package com.example.eastagile.themoviedatabase.base

import android.os.Bundle
import com.example.eastagile.themoviedatabase.R

class TestActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.test_activity)
    }

}
