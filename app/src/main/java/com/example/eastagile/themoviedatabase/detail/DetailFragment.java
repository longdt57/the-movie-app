package com.example.eastagile.themoviedatabase.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.base.BaseViewPagerAdapter;
import com.example.eastagile.themoviedatabase.base.DepthPageTransformer;
import com.example.eastagile.themoviedatabase.detail.fragment.reviews.ReviewFragment;
import com.example.eastagile.themoviedatabase.detail.fragment.trailer.TrailerFragment;
import com.example.eastagile.themoviedatabase.home.MovieActivity;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;

import static com.example.eastagile.themoviedatabase.util.MovieDetailUtils.CURRENT_TAB;
import static com.example.eastagile.themoviedatabase.util.MovieDetailUtils.MOVIE_OBJECT;
import static com.example.eastagile.themoviedatabase.util.MovieUtils.MOVIE_DETAIL_HEADER_TYPE;

public class DetailFragment extends Fragment implements MovieActivity.RightFragmentInterface {

    private final int[] mFragmentsTitle = {
            R.string.title_details,
            R.string.title_reviews
    };
    @VisibleForTesting
    public BaseViewPagerAdapter mAdapter;
    private Movie mMovie;
    private ViewPager mViewPager;
    private TextView movieTabTitle;
    private TextView movieTitle;

    public static DetailFragment newInstance(Movie movie) {
        DetailFragment fragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(MovieDetailUtils.MOVIE_OBJECT, movie);
        fragment.setArguments(bundle);

        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new BaseViewPagerAdapter(getChildFragmentManager());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        movieTabTitle = view.findViewById(R.id.tv_detail_header_type);
        movieTitle = view.findViewById(R.id.tv_detail_header_movie_name);

        if (getArguments() != null) {
            mMovie = getArguments().getParcelable(MOVIE_OBJECT);
            String headerType = getArguments().getString(MOVIE_DETAIL_HEADER_TYPE, "");

            if (!TextUtils.isEmpty(headerType)) {
                view.findViewById(R.id.detail_header).setVisibility(View.VISIBLE);
                movieTabTitle.setText(headerType);
                movieTitle.setText(mMovie.title != null ? mMovie.title.toUpperCase() : "");
            }
        }

        TabLayout mTabLayout = view.findViewById(R.id.tab_layout);
        mViewPager = view.findViewById(R.id.vp_detail);

        TrailerFragment mTrailerFragment = TrailerFragment.newInstance(mMovie);
        ReviewFragment mReviewFragment = ReviewFragment.newInstance(mMovie);

        mAdapter.addFragment(mTrailerFragment, getString(mFragmentsTitle[0]));
        mAdapter.addFragment(mReviewFragment, getString(mFragmentsTitle[1]));

        // Set up view pager
        mViewPager.setAdapter(mAdapter);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mTabLayout.setupWithViewPager(mViewPager);

        mViewPager.setOffscreenPageLimit(2);
        if (savedInstanceState != null) {
            mViewPager.setCurrentItem(savedInstanceState.getInt(CURRENT_TAB, 0));
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_TAB, mViewPager.getCurrentItem());
    }

    @Override
    public void updateMovie(Movie item, String Title) {
        movieTabTitle.setText(Title);
        movieTitle.setText(item.title != null ? item.title.toUpperCase() : "");

        ((ChildFragmentInterface) mAdapter.getItem(0)).updateMovie(item);
        ((ChildFragmentInterface) mAdapter.getItem(1)).updateMovie(item);
    }

    public interface ChildFragmentInterface {
        void updateMovie(Movie movie);
    }
}
