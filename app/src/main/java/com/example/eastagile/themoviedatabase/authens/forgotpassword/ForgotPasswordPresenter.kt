package com.example.eastagile.themoviedatabase.authens.forgotpassword

import com.example.eastagile.themoviedatabase.util.AuthenUtils.ERROR_EMPTY
import com.example.eastagile.themoviedatabase.util.Utils
import com.google.firebase.auth.FirebaseAuth

open class ForgotPasswordPresenter(private var mView: ForgotPasswordContract.View?,
                                   private val mAuth: FirebaseAuth) : ForgotPasswordContract.Presenter {
    override fun stop() {
        mView = null
    }

    override fun start(view: ForgotPasswordContract.View) {
        mView = view
    }

    override fun sendPasswordResetEmail(email: String?) {
        if (Utils.isEmpty(email)) {
            mView?.showError(ERROR_EMPTY)
        } else {
            mAuth.sendPasswordResetEmail(email!!)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mView?.showSuccess()
                        } else {
                            mView?.showError(task.exception?.message.toString())
                        }
                    }
        }
    }
}
