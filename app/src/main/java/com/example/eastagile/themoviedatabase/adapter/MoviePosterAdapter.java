package com.example.eastagile.themoviedatabase.adapter;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.util.Utils;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

/**
 * The movie poster adapter, handle poster views with popular | rate | favorite
 */

public class MoviePosterAdapter extends
        RecyclerView.Adapter<MoviePosterAdapter.MoviePosterViewHolder> {

    private final List<Movie> mList;
    private final ItemClickListener mListener;

    private int mSelectedItem = -1;

    public MoviePosterAdapter(ItemClickListener listener) {
        this.mListener = listener;
        mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MoviePosterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.movie_poster_item, viewGroup, false);
        return new MoviePosterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MoviePosterViewHolder viewHolder, int pos) {
        final Movie item = mList.get(pos);
        viewHolder.tvTitle.setText(item.title);
        viewHolder.imvFavourite.setImageDrawable(
                ContextCompat.getDrawable(viewHolder.itemView.getContext(),
                        item.isFavorite ? R.drawable.btn_favourite_on : R.drawable.btn_favourite_off));

        viewHolder.itemView.setActivated(pos == mSelectedItem);

        viewHolder.loadPosterImage(item.posterPath);

        // Set listener
        viewHolder.itemView.setOnClickListener(view -> {
            mListener.onPosterClick(view, item);
            viewHolder.itemView.setActivated(true);
            int previousSelectedItem = mSelectedItem;
            mSelectedItem = pos;

            // update orange background
            notifyItemChanged(previousSelectedItem);
            notifyItemChanged(viewHolder.getAdapterPosition());

        });

        viewHolder.imvFavourite.setOnClickListener(view -> {
            item.isFavorite = !item.isFavorite;
            Animation scale = AnimationUtils.loadAnimation(viewHolder.itemView.getContext(), R.anim.imv_favorite);
            viewHolder.imvFavourite.startAnimation(scale);
            notifyItemChanged(viewHolder.getAdapterPosition());
            mListener.onFavoriteClick(item, Utils.getFavoriteDatabaseReference());
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public List<Movie> getList() {
        return mList;
    }

    public int getSelectedItem() {
        return mSelectedItem;
    }

    public void setSelectedItem(int mSelectedItem) {
        this.mSelectedItem = mSelectedItem;
    }

    public interface ItemClickListener {
        void onPosterClick(View view, Movie item);

        void onFavoriteClick(Movie item, DatabaseReference reference);
    }

    class MoviePosterViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        ImageView imvPoster;
        ImageView imvFavourite;
        ImageView imvLoading;

        MoviePosterViewHolder(@NonNull View view) {
            super(view);

            tvTitle = itemView.findViewById(R.id.tv_poster_title);
            imvPoster = itemView.findViewById(R.id.imv_poster);
            imvFavourite = itemView.findViewById(R.id.imv_poster_favourite);
            imvLoading = itemView.findViewById(R.id.imv_loading);

        }

        void loadPosterImage(String url) {
            Utils.loadImage(url, imvPoster, imvLoading, itemView.getContext());
        }
    }
}
