package com.example.eastagile.themoviedatabase.detail.fragment.trailer;

import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.MovieRepo;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;
import com.example.eastagile.themoviedatabase.util.Utils;
import com.google.firebase.database.DatabaseReference;

import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

public class TrailerPresenter implements TrailerContract.Presenter {

    private final MovieRepo mMovieRepo;
    private final Scheduler mMainScheduler;
    private final Scheduler mSubScheduler;
    private Disposable mDisposable;
    private TrailerContract.View mView;
    private boolean mFirstLoad = true;

    public TrailerPresenter(MovieRepo movieRepo, TrailerContract.View trailerViewInterface,
                            Scheduler main, Scheduler sub) {
        this.mMovieRepo = movieRepo;
        this.mView = trailerViewInterface;

        mMainScheduler = main;
        mSubScheduler = sub;
    }

    @Override
    public void loadTrailers(String movieId, boolean forceLoad) {
        if (mFirstLoad || forceLoad) {
            mFirstLoad = false;

            mDisposable = mMovieRepo.getMovieTrailers(movieId)
                    .subscribeOn(mSubScheduler)
                    .observeOn(mMainScheduler)
                    .subscribe(
                            movieTrailerResult -> mView.showTrailers(
                                    MovieDetailUtils.convertTrailers(movieTrailerResult)),
                            throwable -> mView.showError(throwable.getMessage())

                    );
        }
    }

    @Override
    public void onFavoriteClick(Movie item, boolean isLogin, DatabaseReference reference) {
        Utils.onFavoriteClick(item, isLogin, reference, mMovieRepo, mDisposable, mMainScheduler, mSubScheduler, mView);
    }

    @Override
    public void start(TrailerContract.View view) {
        mView = view;
    }

    @Override
    public void stop() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
        mView = null;
    }
}
