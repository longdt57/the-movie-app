package com.example.eastagile.themoviedatabase.detail.fragment.reviews;

import com.example.eastagile.themoviedatabase.base.BasePresenter;
import com.example.eastagile.themoviedatabase.base.BaseView;
import com.example.eastagile.themoviedatabase.model.MovieReview;

import java.util.List;

public interface ReviewContract {

    interface View extends BaseView<Presenter> {
        void showReviews(List<MovieReview> list);
    }

    interface Presenter extends BasePresenter<View> {
        void loadReviews(String movieId, boolean forceLoad);
    }

}
