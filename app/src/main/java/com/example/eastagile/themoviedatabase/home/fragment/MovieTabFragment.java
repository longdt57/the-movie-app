package com.example.eastagile.themoviedatabase.home.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.adapter.MoviePosterAdapter;
import com.example.eastagile.themoviedatabase.base.BaseFragment;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.MovieRepoImpl;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.util.List;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.example.eastagile.themoviedatabase.util.MovieUtils.MOVIE_CURRENT_SELECTED_ITEM;
import static com.example.eastagile.themoviedatabase.util.MovieUtils.MOVIE_TAB;
import static com.example.eastagile.themoviedatabase.util.Utils.getFavoriteDatabaseReference;

/**
 * This fragment handle the home screen movie poster list for Popular, Rate, Favorite
 */

public class MovieTabFragment extends BaseFragment implements MoviePosterAdapter.ItemClickListener,
        MovieTabContract.View {

    // Maximum number of page to scroll down
    private static final int MAX_PAGE = 4;

    // Number of column
    private static final int CONTENT_SPAN_COUNT = 2;

    @VisibleForTesting
    public final MoviePosterAdapter mAdapter = new MoviePosterAdapter(this);

    @VisibleForTesting
    public MovieTabContract.Presenter mPresenter = new MovieTabPresenter(MovieRepoImpl.newInstance(),
            AndroidSchedulers.mainThread(), Schedulers.io());

    @VisibleForTesting
    public PosterClickListener mListener;

    private RecyclerView mRecyclerView;
    private MovieTabType mTabType;
    private int mDataCurrentPage = 1;

    public static MovieTabFragment newInstance(MovieTabType tab) {
        MovieTabFragment fragment = new MovieTabFragment();
        Bundle args = new Bundle();
        args.putInt(MOVIE_TAB, tab.value);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(MOVIE_CURRENT_SELECTED_ITEM, mAdapter.getSelectedItem());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PosterClickListener) {
            mListener = (PosterClickListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int tabTypeValue = 0;
        if (getArguments() != null) {
            tabTypeValue = getArguments().getInt(MOVIE_TAB, 0);
        }

        if (tabTypeValue == MovieTabType.POPULAR.value) {
            mTabType = MovieTabType.POPULAR;
        } else if (tabTypeValue == MovieTabType.MOST_RATE.value) {
            mTabType = MovieTabType.MOST_RATE;
        } else {
            mTabType = MovieTabType.MY_FAVORITE;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start(this);
        mPresenter.loadMovies(mTabType, mAdapter.getList().isEmpty(),
                FirebaseAuth.getInstance().getCurrentUser() != null, getFavoriteDatabaseReference());
        updateFavoriteStatus();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate view
        View view = inflater.inflate(R.layout.fragment_poster, container, false);

        // Back to previous status
        if (savedInstanceState != null) {
            mAdapter.setSelectedItem(savedInstanceState.getInt(MOVIE_CURRENT_SELECTED_ITEM, -1));
        }

        // Set up recycler view
        mRecyclerView = view.findViewById(R.id.rcv_content);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), CONTENT_SPAN_COUNT));
        mRecyclerView.setAdapter(mAdapter);

        // Set up progress indicator
        final SwipeRefreshLayout mSwipeRefreshLayout = initSwipeRefreshLayout(view);
        // Set up pull down to refresh
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mPresenter.loadMovies(mTabType, true,
                    FirebaseAuth.getInstance().getCurrentUser() != null, getFavoriteDatabaseReference());
            mDataCurrentPage = 1;
        });

        // Set up pull to bottom to load more
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0 && mDataCurrentPage < MAX_PAGE) {
                    GridLayoutManager manager = (GridLayoutManager) mRecyclerView.getLayoutManager();
                    if (manager != null) {
                        int visibleItemCount = manager.getChildCount();
                        int totalItemCount = manager.getItemCount();
                        int pastVisibleItems = manager.findFirstVisibleItemPosition();

                        if (!mSwipeRefreshLayout.isRefreshing()) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                mPresenter.loadMoreMovies(mTabType, mDataCurrentPage);
                            }
                        }
                    }

                }
            }
        });

        return view;
    }

    /**
     * Handle Movie item click, move to Detail fragment
     *
     * @param view item click
     * @param item movie data
     */
    @Override
    public void onPosterClick(View view, Movie item) {
        mListener.onPosterClick(item);
    }

    @Override
    public void onFavoriteClick(Movie item, DatabaseReference reference) {
        mPresenter.onFavoriteClick(item, FirebaseAuth.getInstance().getCurrentUser() != null, reference);
    }

    @Override
    public void showMovies(List<Movie> list) {
        mAdapter.getList().clear();
        mAdapter.getList().addAll(list);
        runLayoutAnimation(mRecyclerView);
        mRecyclerView.scrollToPosition(mAdapter.getSelectedItem());

        updateFavoriteStatus();
        setLoadingIndicator(false);

        String message = list.isEmpty() ? getString(R.string.refresh) : "";
        showError(message);
    }

    @Override
    public void showMoreMovies(List<Movie> list, int loadedPage) {
        if (mDataCurrentPage < loadedPage) {
            mAdapter.getList().addAll(list);
            mAdapter.notifyDataSetChanged();
            updateFavoriteStatus();
            mDataCurrentPage = loadedPage;
        }
        setLoadingIndicator(false);

        String message = list.isEmpty() ? getString(R.string.refresh) : "";
        showError(message);
    }

    @Override
    public void updateMovieFavoriteStatus(List<Movie> favoriteList) {
        List<Movie> movieList = mAdapter.getList();
        for (Movie movie : movieList) {
            movie.isFavorite = favoriteList.contains(movie);
        }

        mAdapter.notifyDataSetChanged();
    }

    /**
     * Update favorite status
     */
    private void updateFavoriteStatus() {
        if (!mAdapter.getList().isEmpty() && mTabType != MovieTabType.MY_FAVORITE) {
            mPresenter.getMyFavoriteMoviesWithId(mAdapter.getList(),
                    FirebaseAuth.getInstance().getCurrentUser() != null,
                    getFavoriteDatabaseReference());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.stop();
    }

    /**
     * Handle movie poster item click
     */
    public interface PosterClickListener {
        void onPosterClick(Movie item);
    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

}
