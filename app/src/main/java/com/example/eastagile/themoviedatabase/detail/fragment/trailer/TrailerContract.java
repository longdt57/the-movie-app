package com.example.eastagile.themoviedatabase.detail.fragment.trailer;

import com.example.eastagile.themoviedatabase.base.BasePresenter;
import com.example.eastagile.themoviedatabase.base.BaseView;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.model.MovieTrailer;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

public interface TrailerContract {
    interface View extends BaseView<Presenter> {
        void showTrailers(List<MovieTrailer> list);

    }

    interface Presenter extends BasePresenter<View> {
        void loadTrailers(String movieId, boolean forceLoad);

        void onFavoriteClick(Movie item, boolean isLogin, DatabaseReference reference);
    }

}
