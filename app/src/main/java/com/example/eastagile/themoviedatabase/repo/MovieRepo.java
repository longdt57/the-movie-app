package com.example.eastagile.themoviedatabase.repo;

import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;
import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepoImpl;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Define Movie Repo function
 */

public interface MovieRepo {
    Flowable<List<MoviePosterEntity>> getMoviesFavourite();

    Flowable<List<MoviePosterEntity>> getMoviesFavoriteById(int... movieId);

    Completable addFavoriteMovie(List<MoviePosterEntity> items);

    Completable removeFavoriteMovies(List<MoviePosterEntity> items);

    Completable deleteFavoriteAll();

    Observable<MovieResult> getMoviesBy(MovieRemoteRepoImpl.MovieSortType sortOption);

    Observable<MovieResult> getMoviesBy(MovieRemoteRepoImpl.MovieSortType sortOption, int page);

    Observable<MovieTrailerResult> getMovieTrailers(String movieId);

    Observable<MovieReviewResult> getMovieReviews(String movieId);

}
