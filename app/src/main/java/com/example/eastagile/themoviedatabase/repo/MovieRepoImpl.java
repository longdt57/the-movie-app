package com.example.eastagile.themoviedatabase.repo;

import com.example.eastagile.themoviedatabase.base.MovieApplication;
import com.example.eastagile.themoviedatabase.repo.local.MovieLocalRepo;
import com.example.eastagile.themoviedatabase.repo.local.MovieLocalRepoImpl;
import com.example.eastagile.themoviedatabase.repo.local.MoviePosterEntity;
import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepo;
import com.example.eastagile.themoviedatabase.repo.webservice.MovieRemoteRepoImpl;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieReviewResult;
import com.example.eastagile.themoviedatabase.repo.webservice.result.MovieTrailerResult;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;


public class MovieRepoImpl implements MovieRepo {

    private final MovieLocalRepo localRepo;

    private final MovieRemoteRepo remoteRepo;

    public MovieRepoImpl(MovieLocalRepo localRepo, MovieRemoteRepo remoteRepo) {
        this.localRepo = localRepo;
        this.remoteRepo = remoteRepo;

    }

    public static MovieRepoImpl newInstance() {
        return new MovieRepoImpl(new MovieLocalRepoImpl(MovieApplication.getAppContext()), new MovieRemoteRepoImpl());
    }

    @Override
    public Flowable<List<MoviePosterEntity>> getMoviesFavourite() {
        return localRepo.getAllFavoriteMovies();
    }

    @Override
    public Flowable<List<MoviePosterEntity>> getMoviesFavoriteById(int... movieId) {
        return localRepo.getFavoriteMoviesByIds(movieId);
    }

    @Override
    public Observable<MovieResult> getMoviesBy(MovieRemoteRepoImpl.MovieSortType sortOption) {
        return remoteRepo.getMoviesBy(sortOption);
    }

    @Override
    public Observable<MovieResult> getMoviesBy(MovieRemoteRepoImpl.MovieSortType sortOption, int page) {
        return remoteRepo.getMoviesBy(sortOption, page);
    }

    @Override
    public Observable<MovieTrailerResult> getMovieTrailers(String movieId) {
        return remoteRepo.getMovieTrailers(movieId);
    }

    @Override
    public Observable<MovieReviewResult> getMovieReviews(String movieId) {
        return remoteRepo.getMovieReviews(movieId);
    }

    @Override
    public Completable addFavoriteMovie(List<MoviePosterEntity> items) {
        return localRepo.addFavoriteMovies(items);
    }

    @Override
    public Completable removeFavoriteMovies(List<MoviePosterEntity> items) {
        return localRepo.removeFavoriteMovies(items);
    }

    @Override
    public Completable deleteFavoriteAll() {
        return localRepo.deleteAll();
    }
}
