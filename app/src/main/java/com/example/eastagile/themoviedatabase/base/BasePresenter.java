package com.example.eastagile.themoviedatabase.base;

public interface BasePresenter<T> {

    void start(T view);

    void stop();

}
