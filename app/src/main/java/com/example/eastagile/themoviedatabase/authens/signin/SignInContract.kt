package com.example.eastagile.themoviedatabase.authens.signin

import com.example.eastagile.themoviedatabase.base.BasePresenter
import com.example.eastagile.themoviedatabase.base.BaseView
import com.google.firebase.database.DatabaseReference

interface SignInContract {
    interface View : BaseView<Presenter>

    interface Presenter : BasePresenter<View> {
        fun signIn(email: String?, password: String?, reference: DatabaseReference?)
    }
}
