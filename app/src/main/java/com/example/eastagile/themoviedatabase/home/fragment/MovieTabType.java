package com.example.eastagile.themoviedatabase.home.fragment;

/**
 * Define Movie Tab with position value
 */
public enum MovieTabType {
    POPULAR(0),

    MOST_RATE(1),

    MY_FAVORITE(2);

    public final int value;

    MovieTabType(int position) {
        value = position;
    }
}
