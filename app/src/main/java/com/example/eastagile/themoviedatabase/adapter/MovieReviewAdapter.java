package com.example.eastagile.themoviedatabase.adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.model.MovieReview;

import java.util.ArrayList;
import java.util.List;

public class MovieReviewAdapter extends RecyclerView.Adapter<MovieReviewAdapter.ViewHolder> {

    private final List<MovieReview> dataList;


    public MovieReviewAdapter() {
        this.dataList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.movie_review_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final MovieReview item = dataList.get(i);

        viewHolder.author.setText(item.author != null ? item.author.toUpperCase() : "");
        viewHolder.content.setText(item.content);
        viewHolder.readMore.setVisibility(viewHolder.content.getMaxLines() != Integer.MAX_VALUE ? View.VISIBLE : View.GONE);
        viewHolder.itemView.setOnClickListener(view -> {
            viewHolder.content.setMaxLines(Integer.MAX_VALUE);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public List<MovieReview> getList() {
        return dataList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView author;
        TextView content;
        TextView readMore;

        ViewHolder(View view) {
            super(view);
            author = itemView.findViewById(R.id.tv_review_author);
            content = itemView.findViewById(R.id.tv_review_content);
            readMore = itemView.findViewById(R.id.tv_review_read_more);
        }
    }
}
