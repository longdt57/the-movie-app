package com.example.eastagile.themoviedatabase.repo.local;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface MovieLocalRepo {

    Flowable<List<MoviePosterEntity>> getAllFavoriteMovies();

    Flowable<List<MoviePosterEntity>> getFavoriteMoviesByIds(int... movieIds);

    Completable addFavoriteMovies(List<MoviePosterEntity> items);

    Completable removeFavoriteMovies(List<MoviePosterEntity> items);

    Completable deleteAll();

}
