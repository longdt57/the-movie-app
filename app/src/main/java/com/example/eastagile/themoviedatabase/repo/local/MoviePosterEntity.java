package com.example.eastagile.themoviedatabase.repo.local;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = DBConstant.FAVOURITE_TABLE_NAME)
public class MoviePosterEntity {

    @PrimaryKey
    @ColumnInfo(name = DBConstant.MOVIE_ID)
    public int id;

    @ColumnInfo(name = DBConstant.MOVIE_TITLE)
    public String title;

    @ColumnInfo(name = DBConstant.RELEASE_DATE)
    public String releaseDate;

    @ColumnInfo(name = DBConstant.VOTE_AVERAGE)
    public float voteAverage;

    @ColumnInfo(name = DBConstant.MOVIE_POSTER_PATH)
    public String posterPath;

    @ColumnInfo(name = DBConstant.OVERVIEW)
    public String overview;

}
