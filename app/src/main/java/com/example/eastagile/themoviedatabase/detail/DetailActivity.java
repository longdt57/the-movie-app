package com.example.eastagile.themoviedatabase.detail;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.transition.Fade;
import android.transition.Slide;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.base.BaseActivity;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.util.MovieDetailUtils;

public class DetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Movie movie = getIntent().getParcelableExtra(MovieDetailUtils.MOVIE_OBJECT);
        FragmentManager fm = getSupportFragmentManager();

        if (!TextUtils.isEmpty(movie.title)) {
            initToolbar(movie.title.toUpperCase());
        }

        Fragment fragment = fm.findFragmentById(R.id.fl_detail);
        if (fragment == null) {
            fragment = DetailFragment.newInstance(movie);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_detail, fragment)
                .commit();


        // Transition
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();

            fade.setDuration(1000);
            getWindow().setEnterTransition(fade);

            Slide slide = new Slide();
            slide.setDuration(1000);
            getWindow().setReturnTransition(slide);
        }

    }

}
