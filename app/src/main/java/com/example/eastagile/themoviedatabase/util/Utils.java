package com.example.eastagile.themoviedatabase.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.example.eastagile.themoviedatabase.R;
import com.example.eastagile.themoviedatabase.base.BaseView;
import com.example.eastagile.themoviedatabase.model.Movie;
import com.example.eastagile.themoviedatabase.repo.MovieRepo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

public class Utils {
    public static boolean isEmpty(String text) {
        return text == null || text.equals("");
    }

    private static final int IMAGE_SIZE = 300;

    public static void loadImage(String url, ImageView imvDes, ImageView imvLoading, Context context) {
        if (!Utils.isEmpty(url)) {
            imvLoading.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.progress_animation));
            imvLoading.setVisibility(View.VISIBLE);

            Picasso.get()
                    .load(url)
                    .resize(IMAGE_SIZE, IMAGE_SIZE)
                    .into(imvDes, new Callback() {
                        @Override
                        public void onSuccess() {
                            imvLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            imvLoading.setImageDrawable(ContextCompat.getDrawable(context,
                                    R.drawable.ic_error));
                        }
                    });
        } else {
            imvDes.setImageDrawable(ContextCompat.getDrawable(context,
                    R.drawable.ic_no_image));
        }
    }

    /**
     * Convert date to mmmm yyyy
     *
     * @param dateTime in format example: 2018-09-11
     * @return datetime in format mmmm-yyyy: September 2018
     */
    public static String convertReleaseDate(String dateTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat formatNext = new SimpleDateFormat("MMMM yyyy", Locale.US);
        try {
            Date date = format.parse(dateTime);
            return formatNext.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static void onFavoriteClick(Movie items, boolean isLogin, DatabaseReference reference,
                                MovieRepo mMovieRepo, Disposable disposable, Scheduler mainScheduler,
                                Scheduler subScheduler, BaseView view) {
        if (!isLogin) {
            Completable completable = items.isFavorite ?
                    mMovieRepo.addFavoriteMovie(MovieUtils.convertMovieToEntity(items)) :
                    mMovieRepo.removeFavoriteMovies(MovieUtils.convertMovieToEntity(items));
            disposable = completable.subscribeOn(subScheduler)
                    .observeOn(mainScheduler)
                    .subscribe(() -> view.showSuccess(),
                            throwable -> view.showError(throwable.getMessage()));
        } else {
            reference = reference.child(String.valueOf(items.id));
            if (items.isFavorite) {
                reference.setValue(items, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        view.showSuccess();
                    } else {
                        view.showError(databaseError.getMessage());
                    }

                });
            } else {
                reference.removeValue((databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        view.showSuccess();
                    } else {
                        view.showError(databaseError.getMessage());
                    }
                });
            }
        }
    }

    public static DatabaseReference getFavoriteDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference(FirebaseUtils.FAVORITE_TABLE_NAME);
    }

}
